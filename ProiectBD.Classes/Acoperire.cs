﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectBD.Classes
{
    public class Acoperire
    {
        public string Denumire { get; set; }
        public string dataAcoperire { get; set; }
        public string Descriere { get; set; }
        public string Executant { get; set; }
    }
}
