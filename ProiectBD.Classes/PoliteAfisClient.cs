﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectBD.Classes
{
    public class PoliteAfisClient
    {
        public string NrPolita { get; set; }
        public string DataCreare { get; set; }
        public string DataExpirare { get; set; }
        public string DataEfectiva { get; set; }
        public string Valoare { get; set; }
    }
}
