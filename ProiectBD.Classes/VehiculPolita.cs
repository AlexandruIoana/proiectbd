﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectBD.Classes
{
    public class VehiculPolita
    {
        public string Marca { get; set; }
        public string ModelAuto { get; set; }
        public string SerieSasiu { get; set; }
        public string NrPolita { get; set; }
    }
}
