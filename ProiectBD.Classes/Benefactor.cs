﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectBD.Classes
{
    public class Benefactor
    {
        public string Nume { get; set; }
        public string Prenume { get; set; }
        public string NrPolita { get; set; }
    }
}
