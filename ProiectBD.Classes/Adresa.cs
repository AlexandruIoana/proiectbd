﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectBD.Classes
{
    public class Adresa
    {
        public int ?Id { get; set; }
        public int ?ClientId { get; set; }
        public string Tara { get; set; }
        public string Judet { get; set; }
        public string Localitate { get; set; }
        public string Strada { get; set; }
        public int ?Numar { get; set; }
        public int ?Bloc { get; set; }
        public char ?Scara { get; set; }
        public int ?Etaj { get; set; }
        public int ?Apartament { get; set; }

        public Adresa()
        {

        }

        public Adresa(int id,int clientId,string tara,string judet,string localitate,string strada,int numar,int bloc,char scara,int etaj,int apartament)
        {
            this.Id = id;
            this.ClientId = clientId;
            this.Tara = tara;
            this.Judet = judet;
            this.Localitate = localitate;
            this.Strada = strada;
            this.Numar = numar;
            this.Bloc = bloc;
            this.Scara = scara;
            this.Etaj = etaj;
            this.Apartament = apartament;
        }
    }
}
