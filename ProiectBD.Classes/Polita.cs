﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectBD.Classes
{
    public class Polita
    {
        public int ?Id { get; set; }
        public string NumarPolita { get; set; }
        public string DataEfectiva { get; set; }
        public string DataExpirare { get; set; }
        public double ValoareTotala { get; set; }
        public string DataCreare { get; set; }
        public int ?ClientId { get; set; }

        public Polita()
        {

        }

        public Polita(int id,string numarPolita,string dataEfectiva,string dataExpirare,double valoareTotala,string dataCreare,int clientId)
        {
            this.Id = Id;
            this.NumarPolita = numarPolita;
            this.DataEfectiva = dataEfectiva;
            this.DataExpirare = dataExpirare;
            this.ValoareTotala = valoareTotala;
            this.DataCreare = dataCreare;
            this.ClientId = clientId;
        }
    }
}
