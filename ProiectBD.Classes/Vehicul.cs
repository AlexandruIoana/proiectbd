﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectBD.Classes
{
    public class Vehicul
    {
        public int ?Id { get; set; }
        public string Marca { get; set; }
        public string ModelAuto { get; set; }
        public string NumarDeInmatriculare { get; set; }
        public string SerieSasiu { get; set; }
        public int AnulProductiei { get; set; }
        public int MasaMaxima { get; set; }
        public double CapacitateCilindrica { get; set; }
        public int Putere { get; set; }
        public string TipDeCombustibil { get; set; }
        public int NumarDeLocuri { get; set; }
        public string SerieCIV { get; set; }
        public int ?ProprietarId { get; set; }

        public Vehicul()
        {

        }

        public Vehicul(int id,string marca,string modelAuto,string numarDeInmatriculare,string serieSasiu,int anulProductiei,int masaMaxima,double capacitateCilindrica,int putere,string tipDeCombustibil,int numarDeLocuri,string serieCIV,int proprietarId)
        {
            this.Id = id;
            this.Marca = marca;
            this.ModelAuto = modelAuto;
            this.NumarDeInmatriculare = numarDeInmatriculare;
            this.SerieSasiu = serieSasiu;
            this.AnulProductiei = anulProductiei;
            this.MasaMaxima = masaMaxima;
            this.CapacitateCilindrica = capacitateCilindrica;
            this.Putere = putere;
            this.TipDeCombustibil = tipDeCombustibil;
            this.NumarDeLocuri = numarDeLocuri;
            this.SerieCIV = serieCIV;
            this.ProprietarId = proprietarId;
        }
    }
}
