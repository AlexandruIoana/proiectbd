﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectBD.Classes
{
    public class AfisPolita
    {
        public string NrPolita { get; set; }
        public string DataExpirare { get; set; }
        public string Nume { get; set; }
        public string CNP { get; set; }
    }
}
