﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectBD.Classes
{
    public class Client
    {
        public int ?Id { get; set; }
        public string Nume { get; set; }
        public string Prenume { get; set; }
        public string CNP { get; set; }
        public int AnulObtineriiPermisului { get; set; }
        public string Email { get; set; }
        public string NumarTelefon { get; set; }
        public Adresa Adresa { get; set; }

        public Client()
        {
       
        }

        public Client(int id, string nume,string prenume,string cnp,int anulObtineriiPermisului,string email,string numarTelefon,Adresa adresa)
        {
            this.Id = id;
            this.Nume = nume;
            this.Prenume = prenume;
            this.CNP = cnp;
            this.AnulObtineriiPermisului = anulObtineriiPermisului;
            this.Email = email;
            this.NumarTelefon = numarTelefon;
            this.Adresa = adresa;
        }
    }
}
