﻿using ProiectBD.Classes;
using ProiectBD.Service;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ProiectBD.Client
{
    public partial class Form1 : Form
    {
        private List<Vehicul> lista;
        private Rca_Service service;
        private List<string> listaPoliteAcoperire { get; set; }

        private int nr;
        private int numarPoliteAcoperire;
        public Form1()
        {
            InitializeComponent();
            groupBox1.Hide();
            groupBox2.Hide();
            groupBox3.Hide();
            groupBox4.Hide();
            groupBox7.Hide();
            lista = new List<Vehicul>();
            listaPoliteAcoperire = new List<string>();
            service = new Rca_Service();
            nr = 0;
            label43.Text = nr.ToString();
            numarPoliteAcoperire = 0;
            label59.Text = numarPoliteAcoperire.ToString();
            Adauga.Hide();
        }

        private void an_obt_permis_TextChanged(object sender, EventArgs e)
        {
            int temp;
            if (!int.TryParse(an_obt_permis.Text, out temp))
            {
                MessageBox.Show("Ceea ce ai introdus nu este un numar.");
            }
            else
            {
                if (temp < 0)
                    MessageBox.Show("Anul nu are cum sa fie un numar mai mic ca 0.");
            }
        }

        private void numar_TextChanged(object sender, EventArgs e)
        {
            int temp;
            if (!int.TryParse(numar.Text, out temp))
            {
                MessageBox.Show("Ceea ce ai introdus nu este un numar.");
            }
            else
            {
                if (temp < 0)
                    MessageBox.Show("Numarul nu poate sa fie un numar mai mic ca 0.");
            }
        }

        private void bloc_TextChanged(object sender, EventArgs e)
        {
            int temp;
            if (!int.TryParse(bloc.Text, out temp))
            {
                MessageBox.Show("Ceea ce ai introdus nu este un numar.");
            }
            else
            {
                if (temp < 0)
                    MessageBox.Show("Numarul blocul nu  poate sa fie un numar mai mic ca 0.");
            }
        }

        private void etaj_TextChanged(object sender, EventArgs e)
        {
            int temp;
            if (!int.TryParse(etaj.Text, out temp))
            {
                MessageBox.Show("Ceea ce ai introdus nu este un numar.");
            }
            else
            {
                if (temp < 0)
                    MessageBox.Show("Etajul nu are cum sa fie un numar mai mic ca 0.");
            }
        }

        private void anulFabricatiei_TextChanged(object sender, EventArgs e)
        {
            int temp;
            if (!int.TryParse(anulFabricatiei.Text, out temp))
            {
                MessageBox.Show("Ceea ce ai introdus nu este un numar.");
            }
            else
            {
                if (temp < 0)
                    MessageBox.Show("Anul nu are cum sa fie un numar mai mic ca 0.");
            }
        }

        private void masaMaxima_TextChanged(object sender, EventArgs e)
        {
            int temp;
            if (!int.TryParse(masaMaxima.Text, out temp))
            {
                MessageBox.Show("Ceea ce ai introdus nu este un numar.");
            }
            else
            {
                if (temp < 0)
                    MessageBox.Show("Masa maxima nu poate sa fie un numar mai mic ca 0.");
            }
        }

        private void capacitateCilindrica_TextChanged(object sender, EventArgs e)
        {
            double temp;
            if (!double.TryParse(capacitateCilindrica.Text, out temp))
            {
                MessageBox.Show("Ceea ce ai introdus nu este un numar.");
            }
            else
            {
                if (temp < 0.0)
                    MessageBox.Show("Masina nu poate sa aibe capacitatea cilindrica mai mica ca 0.");
            }
        }

        private void putere_TextChanged(object sender, EventArgs e)
        {
            int temp;
            if (!int.TryParse(putere.Text, out temp))
            {
                MessageBox.Show("Ceea ce ai introdus nu este un numar.");
            }
            else
            {
                if (temp < 0)
                    MessageBox.Show("Masina nu poate sa aibe puterea un numar mai mic ca 0.");
            }
        }

        private void numarLocuri_TextChanged(object sender, EventArgs e)
        {
            int temp;
            if (!int.TryParse(numarLocuri.Text, out temp))
            {
                MessageBox.Show("Ceea ce ai introdus nu este un numar.");
            }
            else
            {
                if (temp < 0)
                    MessageBox.Show("Masina nu are cum sa aibe un numar de locurimai mic ca 0.");
            }
        }

        private void valoareTotala_TextChanged(object sender, EventArgs e)
        {
            double temp;
            if (!double.TryParse(valoareTotala.Text, out temp))
            {
                MessageBox.Show("Ceea ce ai introdus nu este un numar.");
            }
            else
            {
                if (temp < 0.0)
                    MessageBox.Show("Valoarea unei polite nu are cum sa fie un numar mai mic ca 0.");
            }
        }

        private void cnpClientAfisPolite_TextChanged(object sender, EventArgs e)
        {
            if (cnpClientAfisPolite.Text == "")
            {
                MessageBox.Show("Trebuie sa introduci CNP-ul!!!");
            }
        }

        private void politaDeExtins_TextChanged(object sender, EventArgs e)
        {
            if (politaDeExtins.Text == "")
            {
                MessageBox.Show("Trebuie introdus numarul politei!");
            }
        }

        private void valAdaug_TextChanged(object sender, EventArgs e)
        {
            if (valAdaug.Text == "")
            {
                MessageBox.Show("Trebuie introdusa suma de bani!");
            }
        }

        private void nrPolitaAcoperire_TextChanged(object sender, EventArgs e)
        {
            if (nrPolitaAcoperire.Text == "")
            {
                MessageBox.Show("Trebuie sa introduci numarul politei");
            }
        }

        private void Adauga_Click(object sender, EventArgs e)
        {
            string cnp_client;
           
            label43.Text = nr.ToString();

            Polita polita = new Polita
            {
                NumarPolita = Guid.NewGuid().ToString(),
                DataEfectiva = dataEfectiva.Value.ToString("yyyy-MM-dd"),
                DataExpirare = dataExpirare.Value.ToString("yyyy-MM-dd"),
                ValoareTotala = Convert.ToDouble(valoareTotala.Text),
                DataCreare = DateTime.Today.ToString("yyyy-MM-dd"),
            };
            
            if (radioButton1.Checked == true)
            {
                Adresa adresa = new Adresa();
                adresa.Tara = tara.Text;
                adresa.Judet = judet.Text;
                adresa.Localitate = localitate.Text;
                adresa.Strada = strada.Text;

                if (numar.Text == "")
                    adresa.Numar = null;
                else
                    adresa.Numar = Convert.ToInt32(numar.Text);

                if (bloc.Text == "")
                    adresa.Bloc = null;
                else
                    adresa.Bloc = Convert.ToInt32(bloc.Text);

                if (scara.Text == "")
                    adresa.Scara = null;
                else
                    adresa.Scara = Convert.ToChar(scara.Text);

                if (etaj.Text == "")
                    adresa.Etaj = null;
                else
                    adresa.Etaj = Convert.ToInt32(bloc.Text);

                if (apartament.Text == "")
                    adresa.Apartament = null;
                else
                    adresa.Apartament = Convert.ToInt32(apartament.Text);


                Classes.Client client = new Classes.Client();
                client.Nume = nume.Text;
                client.Prenume = prenume.Text;
                client.CNP = cnp.Text;
                client.AnulObtineriiPermisului = Convert.ToInt32(an_obt_permis.Text);
                client.Email = email.Text;
                client.NumarTelefon = nrTelefon.Text;
                client.Adresa = adresa;

                try
                {
                    service.AchizitiePolitaClientNou(client, lista, polita);
                    MessageBox.Show("Date Salvate");
                    lista.Clear();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

            
            if (radioButton2.Checked == true)
            {
                cnp_client = cnp_client_existent.Text;
                nr = 0;
                try
                {
                    service.AchizitiePolitaClientExistent(cnp_client, lista, polita);
                    MessageBox.Show("Date Salvate");
                    lista.Clear();
                }
                catch(NonExistentUserException ex)
                {
                    groupBox1.Show();
                    groupBox2.Show();
                    groupBox7.Hide();
                    MessageBox.Show(ex.Message);
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }   
            
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            groupBox7.Hide();
            if (radioButton1.Checked == true)
            {
                groupBox1.Show();
                groupBox2.Show();
                groupBox3.Show();
                groupBox4.Show();
                Adauga.Show();
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            groupBox1.Hide();
            groupBox2.Hide();
            if (radioButton2.Checked == true)
            {
                groupBox7.Show();
                groupBox3.Show();
                groupBox4.Show();
                Adauga.Show();
            }
        }

        private void adaugaVehicul_Click(object sender, EventArgs e)
        {
            Vehicul vehicul = new Vehicul
            {
                Marca = marca.Text,
                ModelAuto = model.Text,
                NumarDeInmatriculare = numarInmatriculare.Text,
                SerieSasiu = serieSasiu.Text,
                AnulProductiei = Convert.ToInt32(anulFabricatiei.Text),
                MasaMaxima = Convert.ToInt32(masaMaxima.Text),
                CapacitateCilindrica = Convert.ToDouble(capacitateCilindrica.Text),
                Putere = Convert.ToInt32(putere.Text),
                TipDeCombustibil = combustibil.Text,
                NumarDeLocuri = Convert.ToInt32(numarLocuri.Text),
                SerieCIV=serieCIV.Text
            };
            lista.Add(vehicul);
            nr = nr + 1;
            label43.Text = nr.ToString();
        }

        private void afiseazaPoliteClient_Click(object sender, EventArgs e)
        {
            string cnp = cnpClientAfisPolite.Text;
            List<PoliteAfisClient> lista = new List<PoliteAfisClient>();
            try
            {
                NrPolitaAfisat.Text = "";
                dataCreareAfisat.Text = "";
                dataCreareEfectiva.Text = "";
                dataExpirareAfisat.Text = "";
                valoareAfisat.Text = "";
                lista = service.GetPoliteByCnp(cnp);
                foreach (var item in lista)
                {
                    NrPolitaAfisat.Text = NrPolitaAfisat.Text + item.NrPolita + "\r\n";
                    dataCreareAfisat.Text = dataCreareAfisat.Text + item.DataCreare + "\r\n";
                    dataCreareEfectiva.Text = dataCreareEfectiva.Text + item.DataEfectiva + "\r\n";
                    dataExpirareAfisat.Text = dataExpirareAfisat.Text + item.DataExpirare + "\r\n";
                    valoareAfisat.Text = valoareAfisat.Text + item.Valoare  + " lei"+ "\r\n";
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void PrelungirePerioada_Click(object sender, EventArgs e)
        {
            string nrPolita = politaDeExtins.Text;
            string nouaData = perioadaNouaExt.Value.ToString("yyyy-MM-dd");
            double addValoare = Convert.ToDouble(valAdaug.Text);
            try
            {
                service.ExtindeDataExpirarePolita(nrPolita, nouaData, addValoare);
                MessageBox.Show("Date Salvate");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AdaugaPolitaAcoperire_Click(object sender, EventArgs e)
        {
            
            string nrPolitaAc = nrPolitaAcoperire.Text;
            if(listaPoliteAcoperire.Find(item => item==nrPolitaAc)!=null)
            {
                MessageBox.Show("Aceasta polita a fost introdusa deja!!");
            }
            else
            {
                listaPoliteAcoperire.Add(nrPolitaAc);
                numarPoliteAcoperire = numarPoliteAcoperire + 1;
                label59.Text = numarPoliteAcoperire.ToString();
            } 
        }

        private void utilizeazaPolita_Click(object sender, EventArgs e)
        {
            Acoperire acoperire = new Acoperire();
            acoperire.Denumire = denumireAcoperire.Text;
            acoperire.dataAcoperire = dataAcoperire.Value.ToString("yyyy-MM-dd");
            acoperire.Descriere = descriereAcoperire.Text;
            acoperire.Executant = textBox49.Text;

            Beneficiar beneficiar = new Beneficiar();
            beneficiar.Nume = numeBeneficiar.Text;
            beneficiar.Prenume = prenumeBeneficiar.Text;
            beneficiar.CNP = cnpBeneficiar.Text;
            beneficiar.Email = EmailBeneficiar.Text;
            beneficiar.NumarTelefon = nrTelefonBeneficiar.Text;

            if (listaPoliteAcoperire.Count>=1)
            {
                try
                {
                    service.UtilizeazaPolita(listaPoliteAcoperire, acoperire, beneficiar);
                    MessageBox.Show("Date Salvate");
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Inca nu ai introdus vreo polita!!");
            }
        }

        private void Afiseaza_Click(object sender, EventArgs e)
        {
            textBox1.Text = ""; textBox2.Text = ""; textBox3.Text = ""; textBox4.Text = ""; textBox5.Text = "";
            textBox6.Text = ""; textBox7.Text = ""; textBox8.Text = ""; textBox9.Text = ""; textBox10.Text = "";
            textBox11.Text = ""; textBox12.Text = ""; textBox13.Text = ""; textBox14.Text = ""; textBox15.Text = "";
            textBox16.Text = ""; textBox17.Text = ""; textBox18.Text = ""; textBox19.Text = ""; textBox20.Text = "";
            textBox21.Text = ""; textBox22.Text = ""; 

            Classes.Client client = new Classes.Client();
            Adresa adresa = new Adresa();
            service.GetDataClient(cnp_detalii.Text, ref client, ref adresa);

            textBox1.Text = client.Nume;
            textBox2.Text = client.Prenume;
            textBox3.Text = client.AnulObtineriiPermisului.ToString();
            textBox4.Text = client.Email;
            textBox5.Text = client.NumarTelefon;

            textBox6.Text = adresa.Tara;
            textBox7.Text = adresa.Judet;
            textBox8.Text = adresa.Localitate;
            textBox9.Text = adresa.Strada;

            if (adresa.Numar == 0)
                textBox10.Text = "-";
            else
                textBox10.Text = adresa.Numar.ToString();

            if (adresa.Bloc == 0)
                textBox11.Text = "-";
            else
                textBox11.Text = adresa.Bloc.ToString();

            if (adresa.Scara == ' ')
                textBox12.Text = "-";
            else
                textBox12.Text = adresa.Scara.ToString();

            if (adresa.Etaj == 0)
                textBox13.Text = "-";
            else
                textBox13.Text = adresa.Etaj.ToString();

            if (adresa.Apartament == 0)
                textBox14.Text = "-";
            else
                textBox14.Text = adresa.Apartament.ToString();

            afisPoliteVehicule(cnp_detalii.Text);

            string command = String.Format("SELECT B.Nume,B.Prenume,B.CNP,B.NumarTelefon FROM[BazaDeDateProiectBD].[dbo].[Beneficiar] B, [BazaDeDateProiectBD].[dbo].[Acoperire] A, [BazaDeDateProiectBD].[dbo].[Polite_Acoperire] PA, [BazaDeDateProiectBD].[dbo].[Polite] P, [BazaDeDateProiectBD].[dbo].[Clienti_PersoanaFizica] C where B.Id = A.BeneficiarId and A.Id = PA.AcoperireId and PA.PolitaId = P.Id and P.ClientId = C.Id and C.CNP = '{0}'", cnp_detalii.Text);


            SqlConnection con;
            SqlDataReader dr;
            SqlCommand cmd;
            string connectionString;
            connectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            con = new SqlConnection(connectionString);

            try
            {
                con.Open();
                cmd = new SqlCommand(command, con);
                dr = cmd.ExecuteReader();
                if (dr.HasRows == true)
                {
                    while (dr.Read())
                    {
                        textBox19.Text = textBox19.Text + dr[0].ToString() + "\r\n";
                        textBox20.Text = textBox20.Text + dr[1].ToString() + "\r\n";
                        textBox21.Text = textBox21.Text + dr[2].ToString() + "\r\n";
                        textBox22.Text = textBox22.Text + dr[3].ToString() + "\r\n";
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            textBox23.Text = "";
            textBox24.Text = "";
            textBox25.Text = "";

            SqlConnection con;
            SqlDataReader dr;
            SqlCommand cmd;
            string connectionString;
            connectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            con = new SqlConnection(connectionString);
            string command = "SELECT distinct C.Nume,C.Prenume,C.CNP FROM[BazaDeDateProiectBD].[dbo].[Clienti_PersoanaFizica] C, [BazaDeDateProiectBD].[dbo].[Polite] P, [BazaDeDateProiectBD].[dbo].[Polite_Acoperire] PA WHERE C.Id = P.ClientId and P.Id = PA.PolitaId";
            try
            {
                con.Open();
                cmd = new SqlCommand(command, con);
                dr = cmd.ExecuteReader();
                if (dr.HasRows == true)
                {
                    while (dr.Read())
                    {
                        textBox23.Text = textBox23.Text + dr[0].ToString() + "\r\n";
                        textBox24.Text = textBox24.Text + dr[1].ToString() + "\r\n";
                        textBox25.Text = textBox25.Text + dr[2].ToString() + "\r\n";
                    }
                }
             
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox26.Text = "";
            textBox27.Text = "";
            textBox28.Text = "";
            SqlConnection con;
            SqlDataReader dr;
            SqlCommand cmd;
            string connectionString;
            connectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";


            string cnp_benef = CNP_benef_Afis.Text;
            string command = String.Format("SELECT C.Nume,C.Prenume,P.NumarPolita FROM[BazaDeDateProiectBD].[dbo].[Clienti_PersoanaFizica] C, [BazaDeDateProiectBD].[dbo].[Polite] P, [BazaDeDateProiectBD].[dbo].[Polite_Acoperire] PA, [BazaDeDateProiectBD].[dbo].[Acoperire] A, [BazaDeDateProiectBD].[dbo].[Beneficiar] B WHERE B.Id = A.BeneficiarId and PA.AcoperireId = A.Id and PA.PolitaId = P.Id and P.ClientId = C.Id and B.CNP = '{0}'", cnp_benef);

            con = new SqlConnection(connectionString);
            try
            {
                con.Open();
                cmd = new SqlCommand(command, con);
                dr = cmd.ExecuteReader();
                if (dr.HasRows == true)
                {
                    while (dr.Read())
                    {
                        textBox26.Text = textBox26.Text + dr[0].ToString() + "\r\n";
                        textBox27.Text = textBox27.Text + dr[1].ToString() + "\r\n";
                        textBox28.Text = textBox28.Text + dr[2].ToString() + "\r\n";
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }

          
        }

        private void button3_Click(object sender, EventArgs e)
        {
           
            afisPoliteExpirate();
            afisPoliteNeExpirate();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            service.EliminatePoliteExpirate();
            MessageBox.Show("Politele Expirate Au fost Sterse");
        }

        ///////////////////////////////////////////
        private void afisPoliteExpirate()
        {
            textBox29.Text = "";
            textBox30.Text = "";
            textBox31.Text = "";
            textBox32.Text = "";

            SqlConnection con;
              SqlDataReader dr;
              SqlCommand cmd;
        string connectionString;
        connectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
     
            con = new SqlConnection(connectionString);
        string command = "SELECT P.NumarPolita,P.DataExpirare,C.Nume,C.Prenume,C.CNP FROM[BazaDeDateProiectBD].[dbo].[Clienti_PersoanaFizica] C, [BazaDeDateProiectBD].[dbo].[Polite] P WHERE P.ClientId=C.Id and P.DataExpirare<CONVERT(date, getdate())";

            try
            {
                con.Open();
                cmd = new SqlCommand(command, con);
                dr = cmd.ExecuteReader();
                if (dr.HasRows == true)
                {
                    while (dr.Read())
                    {
                        textBox29.Text = textBox29.Text + dr[0].ToString() + "\r\n";
                        textBox30.Text = textBox30.Text + dr[1].ToString().Substring(0, dr[1].ToString().IndexOf(" ")) + "\r\n";
                        textBox31.Text = textBox31.Text + dr[2].ToString() + " " + dr[3].ToString() + "\r\n";
                        textBox32.Text = textBox32.Text + dr[4].ToString() + "\r\n";
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }
        private void afisPoliteNeExpirate()
        {
            textBox33.Text = "";
            textBox34.Text = "";
            textBox35.Text = "";
            textBox36.Text = "";
            SqlConnection con;
            SqlDataReader dr;
            SqlCommand cmd;
            string connectionString;
            connectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            con = new SqlConnection(connectionString);
            string command = "SELECT P.NumarPolita,P.DataExpirare,C.Nume,C.Prenume,C.CNP FROM[BazaDeDateProiectBD].[dbo].[Clienti_PersoanaFizica] C, [BazaDeDateProiectBD].[dbo].[Polite] P WHERE P.ClientId=C.Id and P.DataExpirare>CONVERT(date, getdate())";

            try
            {
                con.Open();
                cmd = new SqlCommand(command, con);
                dr = cmd.ExecuteReader();
                if (dr.HasRows == true)
                {
                    while (dr.Read())
                    {
                        textBox33.Text = textBox33.Text + dr[0].ToString() + "\r\n";
                        textBox34.Text = textBox34.Text + dr[1].ToString().Substring(0, dr[1].ToString().IndexOf(" ")) + "\r\n";
                        textBox35.Text = textBox35.Text + dr[2].ToString() + " " + dr[3].ToString() + "\r\n";
                        textBox36.Text = textBox36.Text + dr[4].ToString() + "\r\n";
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private void afisPoliteVehicule(string cnp_Client)
        {
            SqlConnection con;
            SqlDataReader dr;
            SqlCommand cmd;
            string connectionString;
            connectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            con = new SqlConnection(connectionString);

            string command;
            command = string.Format("SELECT V.Marca,V.ModelAuto,V.SerieSasiu,P.NumarPolita FROM[BazaDeDateProiectBD].[dbo].[Vehicule] V, [BazaDeDateProiectBD].[dbo].[Polite] P,[BazaDeDateProiectBD].[dbo].[Polite_Vehicule] PV ,[BazaDeDateProiectBD].[dbo].[Clienti_PersoanaFizica] C where C.Id = V.ProprietarId and V.Id = PV.VehiculId and PV.PolitaId = P.Id and C.CNP = '{0}'", cnp_Client);
            
            try
            {
                con.Open();
                cmd = new SqlCommand(command, con);
                dr = cmd.ExecuteReader();
                if (dr.HasRows == true)
                {
                    while (dr.Read())
                    {
                        textBox15.Text = textBox15.Text + dr[0].ToString() + "\r\n";
                        textBox16.Text = textBox16.Text + dr[1].ToString() + "\r\n";
                        textBox17.Text = textBox17.Text + dr[2].ToString() + "\r\n";
                        textBox18.Text = textBox18.Text + dr[3].ToString() + "\r\n";
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            lista.Clear();
            nr = 0;
            label43.Text = nr.ToString();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBox38.Text = "";
            string nrPolita = textBox37.Text;

            SqlConnection con;
            SqlDataReader dr;
            SqlCommand cmd;
            string connectionString;
            connectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            con = new SqlConnection(connectionString);

            string command;
            command = string.Format("SELECT A.NumeAcoperire,A.Data,A.Descriere,A.Executant FROM[BazaDeDateProiectBD].[dbo].[Acoperire] A WHERE A.Id in (SELECT PA.AcoperireId from[BazaDeDateProiectBD].[dbo].[Polite_Acoperire] PA,[BazaDeDateProiectBD].[dbo].[Polite] P  where P.Id = PA.PolitaId and P.NumarPolita = '{0}')", nrPolita);
            try
            {
                con.Open();
                cmd = new SqlCommand(command, con);
                dr = cmd.ExecuteReader();
                if (dr.HasRows == true)
                {
                    while (dr.Read())
                    {
                        textBox38.Text = textBox38.Text + "Eveniment: " + dr[0].ToString() + "\r\n";
                        textBox38.Text = textBox38.Text + "Data: " + dr[1].ToString().Substring(0, dr[1].ToString().IndexOf(" ")) + "\r\n";
                        textBox38.Text = textBox38.Text + "Executant: " + dr[3].ToString() + "\r\n" ;
                        textBox38.Text = textBox38.Text + "Descriere: " + dr[2].ToString() + "\r\n" + "\r\n";
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            string nrPolita = textBox37.Text;
            string command;
            command = string.Format("DELETE FROM[BazaDeDateProiectBD].[dbo].[Acoperire] WHERE Id in (SELECT PA.AcoperireId from[BazaDeDateProiectBD].[dbo].[Polite_Acoperire] PA, [BazaDeDateProiectBD].[dbo].[Polite] P  where P.Id = PA.PolitaId and P.NumarPolita = '{0}')", nrPolita);
            Rca_Service service = new Rca_Service();
            service.executeQuery(command);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            string command = string.Format("UPDATE [BazaDeDateProiectBD].[dbo].[Clienti_Adrese] SET[Tara] = '{0}' ,[Judet] = '{1}',[Localitate] = '{2}' , [Strada] = '{3}' , [Numar] = {4} , [Bloc] = {5} , [Scara] = '{6}' , [Etaj] = {7}, [Apartament] = {8} WHERE ClientId = (SELECT Id FROM[BazaDeDateProiectBD].[dbo].[Clienti_PersoanaFizica]WHERE CNP = '{9}')",textBox48.Text,textBox47.Text, textBox46.Text, textBox45.Text, (textBox44.Text == "") ? DBNull.Value.ToString() : textBox44.Text, (textBox43.Text == "") ? DBNull.Value.ToString() : textBox43.Text, (textBox42.Text == "") ? DBNull.Value.ToString() : textBox42.Text, (textBox41.Text=="")? DBNull.Value.ToString(): textBox41.Text, textBox40.Text, textBox39.Text);

            Rca_Service service = new Rca_Service();
            service.executeQuery(command);
        }

        private void fillTable(string comand)
        {
            dataGridView1.AutoResizeColumns();
            string connectionString;
            connectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            using (SqlConnection c = new SqlConnection(connectionString))
            {
                using (SqlDataAdapter a = new SqlDataAdapter(comand, c))
                {
                    DataTable t = new DataTable();
                    a.Fill(t);
                    dataGridView1.DataSource = t;
                }
            }
        }
        private void button9_Click(object sender, EventArgs e)
        {
            string command = "SELECT Nume,Prenume,CNP  FROM [BazaDeDateProiectBD].[dbo].Beneficiar where Id in (SELECT BeneficiarId FROM [BazaDeDateProiectBD].[dbo].Acoperire group by BeneficiarId having count(BeneficiarId)>=2)";

            fillTable(command);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            string command = "SELECT C.Nume,C.Prenume,C.CNP,P.NumarPolita from [BazaDeDateProiectBD].[dbo].Clienti_PersoanaFizica C,[BazaDeDateProiectBD].[dbo].Polite P where C.Id=P.ClientId and P.id not in (select PolitaID from [BazaDeDateProiectBD].[dbo].Polite_Acoperire)";

            fillTable(command);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            string command = "SELECT P.NumarPolita,P.DataExpirare,C.Nume,C.Prenume,C.CNP FROM[BazaDeDateProiectBD].[dbo].[Polite] P, [BazaDeDateProiectBD].[dbo].[Clienti_PersoanaFizica] C WHERE P.ClientId=C.Id and P.Id in (SELECT PolitaId from [BazaDeDateProiectBD].[dbo].Polite_Acoperire group by PolitaID having COUNT(PolitaId)>=2)";

            fillTable(command);
        }

        private void textBox50_TextChanged(object sender, EventArgs e)
        {
            if(textBox50.Text=="")
            {
                MessageBox.Show("Nu ai introdus seria sasiului!!");
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            string serieSasiul = textBox50.Text;
            string command = string.Format("SELECT P.NumarPolita,P.DataEfectiva,P.DataExpirare,P.ValoareTotala,C.Nume,C.Prenume,C.CNP FROM [BazaDeDateProiectBD].[dbo].Clienti_PersoanaFizica C, [BazaDeDateProiectBD].[dbo].[Polite] P, [BazaDeDateProiectBD].[dbo].[Vehicule] V,[BazaDeDateProiectBD].[dbo].Polite_Vehicule PV WHERE V.SerieSasiu='{0}' and V.Id=PV.VehiculId and PV.PolitaId=P.Id and V.ProprietarId=C.Id",serieSasiul);
            fillTable(command);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            listaPoliteAcoperire.Clear();
            numarPoliteAcoperire = 0;
            label59.Text = numarPoliteAcoperire.ToString();
        }
    }
}
