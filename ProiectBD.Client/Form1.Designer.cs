﻿namespace ProiectBD.Client
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.an_obt_permis = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.nrTelefon = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.email = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cnp = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.prenume = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.nume = new System.Windows.Forms.TextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label37 = new System.Windows.Forms.Label();
            this.cnp_client_existent = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label41 = new System.Windows.Forms.Label();
            this.valoareTotala = new System.Windows.Forms.TextBox();
            this.dataCreare = new System.Windows.Forms.DateTimePicker();
            this.dataExpirare = new System.Windows.Forms.DateTimePicker();
            this.dataEfectiva = new System.Windows.Forms.DateTimePicker();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.Adauga = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button5 = new System.Windows.Forms.Button();
            this.label43 = new System.Windows.Forms.Label();
            this.nrVehicule = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.adaugaVehicul = new System.Windows.Forms.Button();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.anulFabricatiei = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.serieCIV = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.numarLocuri = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.combustibil = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.putere = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.capacitateCilindrica = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.masaMaxima = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.serieSasiu = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.numarInmatriculare = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.model = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.marca = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.apartament = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.etaj = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.scara = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.bloc = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.numar = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.strada = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.localitate = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.judet = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tara = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.valAdaug = new System.Windows.Forms.TextBox();
            this.PrelungirePerioada = new System.Windows.Forms.Button();
            this.label46 = new System.Windows.Forms.Label();
            this.perioadaNouaExt = new System.Windows.Forms.DateTimePicker();
            this.politaDeExtins = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label45 = new System.Windows.Forms.Label();
            this.valoareAfisat = new System.Windows.Forms.TextBox();
            this.dataExpirareAfisat = new System.Windows.Forms.TextBox();
            this.dataCreareEfectiva = new System.Windows.Forms.TextBox();
            this.dataCreareAfisat = new System.Windows.Forms.TextBox();
            this.NrPolitaAfisat = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.afiseazaPoliteClient = new System.Windows.Forms.Button();
            this.cnpClientAfisPolite = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.utilizeazaPolita = new System.Windows.Forms.Button();
            this.AdaugaPolitaAcoperire = new System.Windows.Forms.Button();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.nrPolitaAcoperire = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.nrTelefonBeneficiar = new System.Windows.Forms.TextBox();
            this.EmailBeneficiar = new System.Windows.Forms.TextBox();
            this.cnpBeneficiar = new System.Windows.Forms.TextBox();
            this.prenumeBeneficiar = new System.Windows.Forms.TextBox();
            this.numeBeneficiar = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.dataAcoperire = new System.Windows.Forms.DateTimePicker();
            this.denumireAcoperire = new System.Windows.Forms.TextBox();
            this.descriereAcoperire = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.label84 = new System.Windows.Forms.Label();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.label79 = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.Afiseaza = new System.Windows.Forms.Button();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label65 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.cnp_detalii = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.label99 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.label98 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.label93 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.CNP_benef_Afis = new System.Windows.Forms.TextBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.label92 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label88 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.button8 = new System.Windows.Forms.Button();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.label108 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.label107 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.vScrollBar1 = new System.Windows.Forms.VScrollBar();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.label105 = new System.Windows.Forms.Label();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.label104 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.textBox49 = new System.Windows.Forms.TextBox();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label118 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.label119 = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.label120 = new System.Windows.Forms.Label();
            this.button11 = new System.Windows.Forms.Button();
            this.label121 = new System.Windows.Forms.Label();
            this.button12 = new System.Windows.Forms.Button();
            this.textBox50 = new System.Windows.Forms.TextBox();
            this.button13 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1277, 565);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox7);
            this.tabPage1.Controls.Add(this.groupBox6);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.Adauga);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1269, 539);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Achizitie polita";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.an_obt_permis);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.nrTelefon);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.email);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cnp);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.prenume);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.nume);
            this.groupBox1.Location = new System.Drawing.Point(24, 71);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(393, 243);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Date personale";
            // 
            // an_obt_permis
            // 
            this.an_obt_permis.Location = new System.Drawing.Point(137, 133);
            this.an_obt_permis.Name = "an_obt_permis";
            this.an_obt_permis.Size = new System.Drawing.Size(161, 20);
            this.an_obt_permis.TabIndex = 12;
            this.an_obt_permis.TextChanged += new System.EventHandler(this.an_obt_permis_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nume";
            // 
            // nrTelefon
            // 
            this.nrTelefon.Location = new System.Drawing.Point(137, 200);
            this.nrTelefon.Name = "nrTelefon";
            this.nrTelefon.Size = new System.Drawing.Size(161, 20);
            this.nrTelefon.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Prenume";
            // 
            // email
            // 
            this.email.Location = new System.Drawing.Point(137, 169);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(161, 20);
            this.email.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "CNP";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Anul Obtinerii Permisului";
            // 
            // cnp
            // 
            this.cnp.Location = new System.Drawing.Point(137, 100);
            this.cnp.Name = "cnp";
            this.cnp.Size = new System.Drawing.Size(161, 20);
            this.cnp.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 172);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Email";
            // 
            // prenume
            // 
            this.prenume.Location = new System.Drawing.Point(137, 65);
            this.prenume.Name = "prenume";
            this.prenume.Size = new System.Drawing.Size(161, 20);
            this.prenume.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 207);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Nr. Telefon";
            // 
            // nume
            // 
            this.nume.Location = new System.Drawing.Point(137, 25);
            this.nume.Name = "nume";
            this.nume.Size = new System.Drawing.Size(161, 20);
            this.nume.TabIndex = 6;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label37);
            this.groupBox7.Controls.Add(this.cnp_client_existent);
            this.groupBox7.Location = new System.Drawing.Point(24, 223);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(399, 100);
            this.groupBox7.TabIndex = 58;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Client Existent";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(19, 47);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(29, 13);
            this.label37.TabIndex = 13;
            this.label37.Text = "CNP";
            // 
            // cnp_client_existent
            // 
            this.cnp_client_existent.Location = new System.Drawing.Point(115, 44);
            this.cnp_client_existent.Name = "cnp_client_existent";
            this.cnp_client_existent.Size = new System.Drawing.Size(145, 20);
            this.cnp_client_existent.TabIndex = 58;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.radioButton2);
            this.groupBox6.Controls.Add(this.radioButton1);
            this.groupBox6.Location = new System.Drawing.Point(30, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(200, 59);
            this.groupBox6.TabIndex = 57;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Client Nou / Existent";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(12, 36);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(91, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Client Existent";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(12, 13);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(74, 17);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Client Nou";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label41);
            this.groupBox4.Controls.Add(this.valoareTotala);
            this.groupBox4.Controls.Add(this.dataCreare);
            this.groupBox4.Controls.Add(this.dataExpirare);
            this.groupBox4.Controls.Add(this.dataEfectiva);
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.label29);
            this.groupBox4.Controls.Add(this.label28);
            this.groupBox4.Controls.Add(this.label27);
            this.groupBox4.Location = new System.Drawing.Point(881, 71);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(325, 243);
            this.groupBox4.TabIndex = 56;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Detalii Polita";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(213, 105);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(17, 13);
            this.label41.TabIndex = 8;
            this.label41.Text = "lei";
            // 
            // valoareTotala
            // 
            this.valoareTotala.Location = new System.Drawing.Point(96, 102);
            this.valoareTotala.Name = "valoareTotala";
            this.valoareTotala.Size = new System.Drawing.Size(100, 20);
            this.valoareTotala.TabIndex = 7;
            this.valoareTotala.TextChanged += new System.EventHandler(this.valoareTotala_TextChanged);
            // 
            // dataCreare
            // 
            this.dataCreare.Location = new System.Drawing.Point(96, 135);
            this.dataCreare.Name = "dataCreare";
            this.dataCreare.Size = new System.Drawing.Size(200, 20);
            this.dataCreare.TabIndex = 6;
            // 
            // dataExpirare
            // 
            this.dataExpirare.Location = new System.Drawing.Point(96, 65);
            this.dataExpirare.Name = "dataExpirare";
            this.dataExpirare.Size = new System.Drawing.Size(200, 20);
            this.dataExpirare.TabIndex = 5;
            // 
            // dataEfectiva
            // 
            this.dataEfectiva.Location = new System.Drawing.Point(96, 25);
            this.dataEfectiva.Name = "dataEfectiva";
            this.dataEfectiva.Size = new System.Drawing.Size(200, 20);
            this.dataEfectiva.TabIndex = 4;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(19, 141);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(64, 13);
            this.label30.TabIndex = 3;
            this.label30.Text = "Data Creare";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(14, 105);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(76, 13);
            this.label29.TabIndex = 2;
            this.label29.Text = "Valoare Totala";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(14, 71);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(71, 13);
            this.label28.TabIndex = 1;
            this.label28.Text = "Data Expirare";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(14, 31);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(72, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Data Efectiva";
            // 
            // Adauga
            // 
            this.Adauga.Location = new System.Drawing.Point(1130, 510);
            this.Adauga.Name = "Adauga";
            this.Adauga.Size = new System.Drawing.Size(115, 23);
            this.Adauga.TabIndex = 55;
            this.Adauga.Text = "Adauga";
            this.Adauga.UseVisualStyleBackColor = true;
            this.Adauga.Click += new System.EventHandler(this.Adauga_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button5);
            this.groupBox3.Controls.Add(this.label43);
            this.groupBox3.Controls.Add(this.nrVehicule);
            this.groupBox3.Controls.Add(this.label42);
            this.groupBox3.Controls.Add(this.adaugaVehicul);
            this.groupBox3.Controls.Add(this.label40);
            this.groupBox3.Controls.Add(this.label39);
            this.groupBox3.Controls.Add(this.label38);
            this.groupBox3.Controls.Add(this.anulFabricatiei);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.serieCIV);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.numarLocuri);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.combustibil);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.putere);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.capacitateCilindrica);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.masaMaxima);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.serieSasiu);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.numarInmatriculare);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.model);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.marca);
            this.groupBox3.Location = new System.Drawing.Point(469, 71);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(359, 458);
            this.groupBox3.TabIndex = 54;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Detalii Autovehicul";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(23, 431);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(153, 23);
            this.button5.TabIndex = 63;
            this.button5.Text = "Stergere Vehicule din lista";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(246, 416);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(0, 13);
            this.label43.TabIndex = 62;
            // 
            // nrVehicule
            // 
            this.nrVehicule.AutoSize = true;
            this.nrVehicule.Location = new System.Drawing.Point(253, 416);
            this.nrVehicule.Name = "nrVehicule";
            this.nrVehicule.Size = new System.Drawing.Size(0, 13);
            this.nrVehicule.TabIndex = 61;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(174, 416);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(65, 13);
            this.label42.TabIndex = 60;
            this.label42.Text = "Nr. Vehicule";
            // 
            // adaugaVehicul
            // 
            this.adaugaVehicul.Location = new System.Drawing.Point(23, 407);
            this.adaugaVehicul.Name = "adaugaVehicul";
            this.adaugaVehicul.Size = new System.Drawing.Size(123, 23);
            this.adaugaVehicul.TabIndex = 59;
            this.adaugaVehicul.Text = "Adauga Vehicul in lista";
            this.adaugaVehicul.UseVisualStyleBackColor = true;
            this.adaugaVehicul.Click += new System.EventHandler(this.adaugaVehicul_Click);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(253, 279);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(25, 13);
            this.label40.TabIndex = 57;
            this.label40.Text = "KW";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(253, 245);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(19, 13);
            this.label39.TabIndex = 56;
            this.label39.Text = "cc";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(253, 207);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(20, 13);
            this.label38.TabIndex = 55;
            this.label38.Text = "Kg";
            // 
            // anulFabricatiei
            // 
            this.anulFabricatiei.Location = new System.Drawing.Point(162, 169);
            this.anulFabricatiei.Name = "anulFabricatiei";
            this.anulFabricatiei.Size = new System.Drawing.Size(85, 20);
            this.anulFabricatiei.TabIndex = 54;
            this.anulFabricatiei.TextChanged += new System.EventHandler(this.anulFabricatiei_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(28, 28);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(37, 13);
            this.label16.TabIndex = 32;
            this.label16.Text = "Marca";
            // 
            // serieCIV
            // 
            this.serieCIV.Location = new System.Drawing.Point(159, 372);
            this.serieCIV.Name = "serieCIV";
            this.serieCIV.Size = new System.Drawing.Size(151, 20);
            this.serieCIV.TabIndex = 53;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(28, 65);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(61, 13);
            this.label17.TabIndex = 33;
            this.label17.Text = "Model Auto";
            // 
            // numarLocuri
            // 
            this.numarLocuri.Location = new System.Drawing.Point(159, 345);
            this.numarLocuri.Name = "numarLocuri";
            this.numarLocuri.Size = new System.Drawing.Size(88, 20);
            this.numarLocuri.TabIndex = 52;
            this.numarLocuri.TextChanged += new System.EventHandler(this.numarLocuri_TextChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(28, 102);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(118, 13);
            this.label18.TabIndex = 34;
            this.label18.Text = "Numar De Inmatriculare";
            // 
            // combustibil
            // 
            this.combustibil.FormattingEnabled = true;
            this.combustibil.Items.AddRange(new object[] {
            "Motorina",
            "Benzina"});
            this.combustibil.Location = new System.Drawing.Point(159, 310);
            this.combustibil.Name = "combustibil";
            this.combustibil.Size = new System.Drawing.Size(109, 21);
            this.combustibil.TabIndex = 51;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(28, 138);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(60, 13);
            this.label19.TabIndex = 35;
            this.label19.Text = "Serie Sasiu";
            // 
            // putere
            // 
            this.putere.Location = new System.Drawing.Point(159, 276);
            this.putere.Name = "putere";
            this.putere.Size = new System.Drawing.Size(88, 20);
            this.putere.TabIndex = 50;
            this.putere.TextChanged += new System.EventHandler(this.putere_TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(28, 174);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(79, 13);
            this.label20.TabIndex = 36;
            this.label20.Text = "Anul Fabricatiei";
            // 
            // capacitateCilindrica
            // 
            this.capacitateCilindrica.Location = new System.Drawing.Point(159, 242);
            this.capacitateCilindrica.Name = "capacitateCilindrica";
            this.capacitateCilindrica.Size = new System.Drawing.Size(88, 20);
            this.capacitateCilindrica.TabIndex = 49;
            this.capacitateCilindrica.TextChanged += new System.EventHandler(this.capacitateCilindrica_TextChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(28, 209);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(72, 13);
            this.label21.TabIndex = 37;
            this.label21.Text = "Masa Maxima";
            // 
            // masaMaxima
            // 
            this.masaMaxima.Location = new System.Drawing.Point(159, 206);
            this.masaMaxima.Name = "masaMaxima";
            this.masaMaxima.Size = new System.Drawing.Size(88, 20);
            this.masaMaxima.TabIndex = 48;
            this.masaMaxima.TextChanged += new System.EventHandler(this.masaMaxima_TextChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(28, 245);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(103, 13);
            this.label22.TabIndex = 38;
            this.label22.Text = "Capacitate Cilindrica";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(28, 279);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(38, 13);
            this.label23.TabIndex = 39;
            this.label23.Text = "Putere";
            // 
            // serieSasiu
            // 
            this.serieSasiu.Location = new System.Drawing.Point(162, 133);
            this.serieSasiu.Name = "serieSasiu";
            this.serieSasiu.Size = new System.Drawing.Size(148, 20);
            this.serieSasiu.TabIndex = 46;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(28, 313);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(95, 13);
            this.label24.TabIndex = 40;
            this.label24.Text = "Tip De Combustibil";
            // 
            // numarInmatriculare
            // 
            this.numarInmatriculare.Location = new System.Drawing.Point(162, 97);
            this.numarInmatriculare.Name = "numarInmatriculare";
            this.numarInmatriculare.Size = new System.Drawing.Size(148, 20);
            this.numarInmatriculare.TabIndex = 45;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(28, 348);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(87, 13);
            this.label25.TabIndex = 41;
            this.label25.Text = "Numar De Locuri";
            // 
            // model
            // 
            this.model.Location = new System.Drawing.Point(162, 60);
            this.model.Name = "model";
            this.model.Size = new System.Drawing.Size(148, 20);
            this.model.TabIndex = 44;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(28, 375);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(51, 13);
            this.label26.TabIndex = 42;
            this.label26.Text = "Serie CIV";
            // 
            // marca
            // 
            this.marca.Location = new System.Drawing.Point(162, 25);
            this.marca.Name = "marca";
            this.marca.Size = new System.Drawing.Size(148, 20);
            this.marca.TabIndex = 43;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.apartament);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.etaj);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.scara);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.bloc);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.numar);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.strada);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.localitate);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.judet);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.tara);
            this.groupBox2.Location = new System.Drawing.Point(24, 331);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(393, 202);
            this.groupBox2.TabIndex = 31;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Adresa trecuta in talon";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 36);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Tara";
            // 
            // apartament
            // 
            this.apartament.Location = new System.Drawing.Point(282, 140);
            this.apartament.Name = "apartament";
            this.apartament.Size = new System.Drawing.Size(100, 20);
            this.apartament.TabIndex = 30;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 72);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Judet";
            // 
            // etaj
            // 
            this.etaj.Location = new System.Drawing.Point(282, 106);
            this.etaj.Name = "etaj";
            this.etaj.Size = new System.Drawing.Size(100, 20);
            this.etaj.TabIndex = 29;
            this.etaj.TextChanged += new System.EventHandler(this.etaj_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 109);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Localitate";
            // 
            // scara
            // 
            this.scara.Location = new System.Drawing.Point(282, 72);
            this.scara.Name = "scara";
            this.scara.Size = new System.Drawing.Size(100, 20);
            this.scara.TabIndex = 28;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 145);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Strada";
            // 
            // bloc
            // 
            this.bloc.Location = new System.Drawing.Point(282, 36);
            this.bloc.Name = "bloc";
            this.bloc.Size = new System.Drawing.Size(100, 20);
            this.bloc.TabIndex = 27;
            this.bloc.TextChanged += new System.EventHandler(this.bloc_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 181);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(38, 13);
            this.label11.TabIndex = 17;
            this.label11.Text = "Numar";
            // 
            // numar
            // 
            this.numar.Location = new System.Drawing.Point(75, 178);
            this.numar.Name = "numar";
            this.numar.Size = new System.Drawing.Size(100, 20);
            this.numar.TabIndex = 26;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(223, 36);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(28, 13);
            this.label12.TabIndex = 18;
            this.label12.Text = "Bloc";
            // 
            // strada
            // 
            this.strada.Location = new System.Drawing.Point(75, 142);
            this.strada.Name = "strada";
            this.strada.Size = new System.Drawing.Size(100, 20);
            this.strada.TabIndex = 25;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(223, 75);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "Scara";
            // 
            // localitate
            // 
            this.localitate.Location = new System.Drawing.Point(75, 106);
            this.localitate.Name = "localitate";
            this.localitate.Size = new System.Drawing.Size(100, 20);
            this.localitate.TabIndex = 24;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(223, 109);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(25, 13);
            this.label14.TabIndex = 20;
            this.label14.Text = "Etaj";
            // 
            // judet
            // 
            this.judet.Location = new System.Drawing.Point(75, 69);
            this.judet.Name = "judet";
            this.judet.Size = new System.Drawing.Size(100, 20);
            this.judet.TabIndex = 23;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(223, 147);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(61, 13);
            this.label15.TabIndex = 21;
            this.label15.Text = "Apartament";
            // 
            // tara
            // 
            this.tara.Location = new System.Drawing.Point(75, 33);
            this.tara.Name = "tara";
            this.tara.Size = new System.Drawing.Size(100, 20);
            this.tara.TabIndex = 22;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox8);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.afiseazaPoliteClient);
            this.tabPage2.Controls.Add(this.cnpClientAfisPolite);
            this.tabPage2.Controls.Add(this.label31);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(1269, 539);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Prelungire perioada polita";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label48);
            this.groupBox8.Controls.Add(this.label47);
            this.groupBox8.Controls.Add(this.valAdaug);
            this.groupBox8.Controls.Add(this.PrelungirePerioada);
            this.groupBox8.Controls.Add(this.label46);
            this.groupBox8.Controls.Add(this.perioadaNouaExt);
            this.groupBox8.Controls.Add(this.politaDeExtins);
            this.groupBox8.Controls.Add(this.label32);
            this.groupBox8.Location = new System.Drawing.Point(779, 75);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(396, 298);
            this.groupBox8.TabIndex = 6;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Extindere Valabilitate Polita";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(236, 201);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(17, 13);
            this.label48.TabIndex = 9;
            this.label48.Text = "lei";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(25, 198);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(61, 26);
            this.label47.TabIndex = 8;
            this.label47.Text = "Valoare de \r\nadaugat";
            // 
            // valAdaug
            // 
            this.valAdaug.Location = new System.Drawing.Point(112, 195);
            this.valAdaug.Name = "valAdaug";
            this.valAdaug.Size = new System.Drawing.Size(100, 20);
            this.valAdaug.TabIndex = 7;
            this.valAdaug.TextChanged += new System.EventHandler(this.valAdaug_TextChanged);
            // 
            // PrelungirePerioada
            // 
            this.PrelungirePerioada.Location = new System.Drawing.Point(247, 259);
            this.PrelungirePerioada.Name = "PrelungirePerioada";
            this.PrelungirePerioada.Size = new System.Drawing.Size(128, 23);
            this.PrelungirePerioada.TabIndex = 6;
            this.PrelungirePerioada.Text = "Prelungeste Perioada";
            this.PrelungirePerioada.UseVisualStyleBackColor = true;
            this.PrelungirePerioada.Click += new System.EventHandler(this.PrelungirePerioada_Click);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(36, 98);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(62, 26);
            this.label46.TabIndex = 5;
            this.label46.Text = "Noua Data \r\nde Expirare";
            // 
            // perioadaNouaExt
            // 
            this.perioadaNouaExt.Location = new System.Drawing.Point(112, 104);
            this.perioadaNouaExt.Name = "perioadaNouaExt";
            this.perioadaNouaExt.Size = new System.Drawing.Size(200, 20);
            this.perioadaNouaExt.TabIndex = 4;
            // 
            // politaDeExtins
            // 
            this.politaDeExtins.Location = new System.Drawing.Point(112, 37);
            this.politaDeExtins.Name = "politaDeExtins";
            this.politaDeExtins.Size = new System.Drawing.Size(263, 20);
            this.politaDeExtins.TabIndex = 3;
            this.politaDeExtins.TextChanged += new System.EventHandler(this.politaDeExtins_TextChanged);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(36, 40);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(50, 13);
            this.label32.TabIndex = 1;
            this.label32.Text = "Nr. Polita";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label45);
            this.groupBox5.Controls.Add(this.valoareAfisat);
            this.groupBox5.Controls.Add(this.dataExpirareAfisat);
            this.groupBox5.Controls.Add(this.dataCreareEfectiva);
            this.groupBox5.Controls.Add(this.dataCreareAfisat);
            this.groupBox5.Controls.Add(this.NrPolitaAfisat);
            this.groupBox5.Controls.Add(this.label44);
            this.groupBox5.Controls.Add(this.label36);
            this.groupBox5.Controls.Add(this.label35);
            this.groupBox5.Controls.Add(this.label34);
            this.groupBox5.Controls.Add(this.label33);
            this.groupBox5.Location = new System.Drawing.Point(22, 127);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(708, 386);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Polite Client";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(25, 47);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(652, 13);
            this.label45.TabIndex = 15;
            this.label45.Text = resources.GetString("label45.Text");
            // 
            // valoareAfisat
            // 
            this.valoareAfisat.Location = new System.Drawing.Point(576, 61);
            this.valoareAfisat.Multiline = true;
            this.valoareAfisat.Name = "valoareAfisat";
            this.valoareAfisat.ReadOnly = true;
            this.valoareAfisat.Size = new System.Drawing.Size(101, 303);
            this.valoareAfisat.TabIndex = 14;
            // 
            // dataExpirareAfisat
            // 
            this.dataExpirareAfisat.Location = new System.Drawing.Point(486, 61);
            this.dataExpirareAfisat.Multiline = true;
            this.dataExpirareAfisat.Name = "dataExpirareAfisat";
            this.dataExpirareAfisat.ReadOnly = true;
            this.dataExpirareAfisat.Size = new System.Drawing.Size(68, 303);
            this.dataExpirareAfisat.TabIndex = 13;
            // 
            // dataCreareEfectiva
            // 
            this.dataCreareEfectiva.Location = new System.Drawing.Point(391, 61);
            this.dataCreareEfectiva.Multiline = true;
            this.dataCreareEfectiva.Name = "dataCreareEfectiva";
            this.dataCreareEfectiva.ReadOnly = true;
            this.dataCreareEfectiva.Size = new System.Drawing.Size(69, 303);
            this.dataCreareEfectiva.TabIndex = 12;
            // 
            // dataCreareAfisat
            // 
            this.dataCreareAfisat.Location = new System.Drawing.Point(310, 61);
            this.dataCreareAfisat.Multiline = true;
            this.dataCreareAfisat.Name = "dataCreareAfisat";
            this.dataCreareAfisat.ReadOnly = true;
            this.dataCreareAfisat.Size = new System.Drawing.Size(63, 303);
            this.dataCreareAfisat.TabIndex = 11;
            // 
            // NrPolitaAfisat
            // 
            this.NrPolitaAfisat.Location = new System.Drawing.Point(25, 61);
            this.NrPolitaAfisat.Multiline = true;
            this.NrPolitaAfisat.Name = "NrPolitaAfisat";
            this.NrPolitaAfisat.ReadOnly = true;
            this.NrPolitaAfisat.Size = new System.Drawing.Size(263, 303);
            this.NrPolitaAfisat.TabIndex = 10;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(573, 34);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(43, 13);
            this.label44.TabIndex = 4;
            this.label44.Text = "Valoare";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(483, 34);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(71, 13);
            this.label36.TabIndex = 3;
            this.label36.Text = "Data Expirare";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(388, 34);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(72, 13);
            this.label35.TabIndex = 2;
            this.label35.Text = "Data Efectiva";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(307, 34);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(64, 13);
            this.label34.TabIndex = 1;
            this.label34.Text = "Data Creare";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(116, 34);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(50, 13);
            this.label33.TabIndex = 0;
            this.label33.Text = "Nr. Polita";
            // 
            // afiseazaPoliteClient
            // 
            this.afiseazaPoliteClient.Location = new System.Drawing.Point(153, 87);
            this.afiseazaPoliteClient.Name = "afiseazaPoliteClient";
            this.afiseazaPoliteClient.Size = new System.Drawing.Size(117, 23);
            this.afiseazaPoliteClient.TabIndex = 4;
            this.afiseazaPoliteClient.Text = "Afiseaza Polite Client";
            this.afiseazaPoliteClient.UseVisualStyleBackColor = true;
            this.afiseazaPoliteClient.Click += new System.EventHandler(this.afiseazaPoliteClient_Click);
            // 
            // cnpClientAfisPolite
            // 
            this.cnpClientAfisPolite.Location = new System.Drawing.Point(95, 50);
            this.cnpClientAfisPolite.Name = "cnpClientAfisPolite";
            this.cnpClientAfisPolite.Size = new System.Drawing.Size(175, 20);
            this.cnpClientAfisPolite.TabIndex = 2;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(32, 53);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(29, 13);
            this.label31.TabIndex = 0;
            this.label31.Text = "CNP";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.button13);
            this.tabPage3.Controls.Add(this.utilizeazaPolita);
            this.tabPage3.Controls.Add(this.AdaugaPolitaAcoperire);
            this.tabPage3.Controls.Add(this.label59);
            this.tabPage3.Controls.Add(this.label58);
            this.tabPage3.Controls.Add(this.nrPolitaAcoperire);
            this.tabPage3.Controls.Add(this.label57);
            this.tabPage3.Controls.Add(this.groupBox10);
            this.tabPage3.Controls.Add(this.groupBox9);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1269, 539);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Acoperire polita";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // utilizeazaPolita
            // 
            this.utilizeazaPolita.Location = new System.Drawing.Point(1049, 499);
            this.utilizeazaPolita.Name = "utilizeazaPolita";
            this.utilizeazaPolita.Size = new System.Drawing.Size(139, 23);
            this.utilizeazaPolita.TabIndex = 7;
            this.utilizeazaPolita.Text = "Utilizeaza Polita/Polite";
            this.utilizeazaPolita.UseVisualStyleBackColor = true;
            this.utilizeazaPolita.Click += new System.EventHandler(this.utilizeazaPolita_Click);
            // 
            // AdaugaPolitaAcoperire
            // 
            this.AdaugaPolitaAcoperire.Location = new System.Drawing.Point(259, 436);
            this.AdaugaPolitaAcoperire.Name = "AdaugaPolitaAcoperire";
            this.AdaugaPolitaAcoperire.Size = new System.Drawing.Size(88, 23);
            this.AdaugaPolitaAcoperire.TabIndex = 6;
            this.AdaugaPolitaAcoperire.Text = "Adauga Polita";
            this.AdaugaPolitaAcoperire.UseVisualStyleBackColor = true;
            this.AdaugaPolitaAcoperire.Click += new System.EventHandler(this.AdaugaPolitaAcoperire_Click);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(323, 402);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(41, 13);
            this.label59.TabIndex = 5;
            this.label59.Text = "label59";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(169, 402);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(104, 13);
            this.label58.TabIndex = 4;
            this.label58.Text = "Numar polite utilizate";
            // 
            // nrPolitaAcoperire
            // 
            this.nrPolitaAcoperire.Location = new System.Drawing.Point(155, 363);
            this.nrPolitaAcoperire.Name = "nrPolitaAcoperire";
            this.nrPolitaAcoperire.Size = new System.Drawing.Size(285, 20);
            this.nrPolitaAcoperire.TabIndex = 3;
            this.nrPolitaAcoperire.TextChanged += new System.EventHandler(this.nrPolitaAcoperire_TextChanged);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(34, 371);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(98, 13);
            this.label57.TabIndex = 2;
            this.label57.Text = "Numar Polita/Polite";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.nrTelefonBeneficiar);
            this.groupBox10.Controls.Add(this.EmailBeneficiar);
            this.groupBox10.Controls.Add(this.cnpBeneficiar);
            this.groupBox10.Controls.Add(this.prenumeBeneficiar);
            this.groupBox10.Controls.Add(this.numeBeneficiar);
            this.groupBox10.Controls.Add(this.label56);
            this.groupBox10.Controls.Add(this.label55);
            this.groupBox10.Controls.Add(this.label54);
            this.groupBox10.Controls.Add(this.label53);
            this.groupBox10.Controls.Add(this.label52);
            this.groupBox10.Location = new System.Drawing.Point(620, 155);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(398, 228);
            this.groupBox10.TabIndex = 1;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "DateBeneficiar";
            // 
            // nrTelefonBeneficiar
            // 
            this.nrTelefonBeneficiar.Location = new System.Drawing.Point(178, 162);
            this.nrTelefonBeneficiar.Name = "nrTelefonBeneficiar";
            this.nrTelefonBeneficiar.Size = new System.Drawing.Size(189, 20);
            this.nrTelefonBeneficiar.TabIndex = 9;
            // 
            // EmailBeneficiar
            // 
            this.EmailBeneficiar.Location = new System.Drawing.Point(178, 124);
            this.EmailBeneficiar.Name = "EmailBeneficiar";
            this.EmailBeneficiar.Size = new System.Drawing.Size(189, 20);
            this.EmailBeneficiar.TabIndex = 8;
            // 
            // cnpBeneficiar
            // 
            this.cnpBeneficiar.Location = new System.Drawing.Point(178, 90);
            this.cnpBeneficiar.Name = "cnpBeneficiar";
            this.cnpBeneficiar.Size = new System.Drawing.Size(189, 20);
            this.cnpBeneficiar.TabIndex = 7;
            // 
            // prenumeBeneficiar
            // 
            this.prenumeBeneficiar.Location = new System.Drawing.Point(178, 57);
            this.prenumeBeneficiar.Name = "prenumeBeneficiar";
            this.prenumeBeneficiar.Size = new System.Drawing.Size(189, 20);
            this.prenumeBeneficiar.TabIndex = 6;
            // 
            // numeBeneficiar
            // 
            this.numeBeneficiar.Location = new System.Drawing.Point(178, 25);
            this.numeBeneficiar.Name = "numeBeneficiar";
            this.numeBeneficiar.Size = new System.Drawing.Size(189, 20);
            this.numeBeneficiar.TabIndex = 5;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(21, 169);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(74, 13);
            this.label56.TabIndex = 4;
            this.label56.Text = "NumarTelefon";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(21, 131);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(32, 13);
            this.label55.TabIndex = 3;
            this.label55.Text = "Email";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(21, 93);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(29, 13);
            this.label54.TabIndex = 2;
            this.label54.Text = "CNP";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(18, 63);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(49, 13);
            this.label53.TabIndex = 1;
            this.label53.Text = "Prenume";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(18, 32);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(35, 13);
            this.label52.TabIndex = 0;
            this.label52.Text = "Nume";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.textBox49);
            this.groupBox9.Controls.Add(this.label117);
            this.groupBox9.Controls.Add(this.dataAcoperire);
            this.groupBox9.Controls.Add(this.denumireAcoperire);
            this.groupBox9.Controls.Add(this.descriereAcoperire);
            this.groupBox9.Controls.Add(this.label51);
            this.groupBox9.Controls.Add(this.label50);
            this.groupBox9.Controls.Add(this.label49);
            this.groupBox9.Location = new System.Drawing.Point(37, 30);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(406, 307);
            this.groupBox9.TabIndex = 0;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Acoperire";
            // 
            // dataAcoperire
            // 
            this.dataAcoperire.Location = new System.Drawing.Point(100, 57);
            this.dataAcoperire.Name = "dataAcoperire";
            this.dataAcoperire.Size = new System.Drawing.Size(200, 20);
            this.dataAcoperire.TabIndex = 5;
            // 
            // denumireAcoperire
            // 
            this.denumireAcoperire.Location = new System.Drawing.Point(100, 30);
            this.denumireAcoperire.Name = "denumireAcoperire";
            this.denumireAcoperire.Size = new System.Drawing.Size(200, 20);
            this.denumireAcoperire.TabIndex = 4;
            // 
            // descriereAcoperire
            // 
            this.descriereAcoperire.Location = new System.Drawing.Point(23, 141);
            this.descriereAcoperire.Multiline = true;
            this.descriereAcoperire.Name = "descriereAcoperire";
            this.descriereAcoperire.Size = new System.Drawing.Size(365, 142);
            this.descriereAcoperire.TabIndex = 3;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(7, 125);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(52, 13);
            this.label51.TabIndex = 2;
            this.label51.Text = "Descriere";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(7, 63);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(30, 13);
            this.label50.TabIndex = 1;
            this.label50.Text = "Data";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(7, 33);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(52, 13);
            this.label49.TabIndex = 0;
            this.label49.Text = "Denumire";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox14);
            this.tabPage4.Controls.Add(this.groupBox13);
            this.tabPage4.Controls.Add(this.groupBox12);
            this.tabPage4.Controls.Add(this.Afiseaza);
            this.tabPage4.Controls.Add(this.groupBox11);
            this.tabPage4.Controls.Add(this.cnp_detalii);
            this.tabPage4.Controls.Add(this.label60);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1269, 539);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Detalii client";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.label84);
            this.groupBox14.Controls.Add(this.textBox22);
            this.groupBox14.Controls.Add(this.textBox21);
            this.groupBox14.Controls.Add(this.textBox20);
            this.groupBox14.Controls.Add(this.textBox19);
            this.groupBox14.Controls.Add(this.label83);
            this.groupBox14.Controls.Add(this.label82);
            this.groupBox14.Controls.Add(this.label81);
            this.groupBox14.Controls.Add(this.label80);
            this.groupBox14.Location = new System.Drawing.Point(534, 315);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(544, 201);
            this.groupBox14.TabIndex = 6;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Beneficiari";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(28, 33);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(499, 13);
            this.label84.TabIndex = 8;
            this.label84.Text = "---------------------------------------------------------------------------------" +
    "--------------------------------------------------------------------------------" +
    "---";
            // 
            // textBox22
            // 
            this.textBox22.Location = new System.Drawing.Point(427, 49);
            this.textBox22.Multiline = true;
            this.textBox22.Name = "textBox22";
            this.textBox22.ReadOnly = true;
            this.textBox22.Size = new System.Drawing.Size(100, 136);
            this.textBox22.TabIndex = 7;
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(291, 49);
            this.textBox21.Multiline = true;
            this.textBox21.Name = "textBox21";
            this.textBox21.ReadOnly = true;
            this.textBox21.Size = new System.Drawing.Size(100, 136);
            this.textBox21.TabIndex = 6;
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(153, 49);
            this.textBox20.Multiline = true;
            this.textBox20.Name = "textBox20";
            this.textBox20.ReadOnly = true;
            this.textBox20.Size = new System.Drawing.Size(100, 136);
            this.textBox20.TabIndex = 5;
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(33, 49);
            this.textBox19.Multiline = true;
            this.textBox19.Name = "textBox19";
            this.textBox19.ReadOnly = true;
            this.textBox19.Size = new System.Drawing.Size(100, 136);
            this.textBox19.TabIndex = 4;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(424, 19);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(57, 13);
            this.label83.TabIndex = 3;
            this.label83.Text = "Nr.Telefon";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(288, 20);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(29, 13);
            this.label82.TabIndex = 2;
            this.label82.Text = "CNP";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(150, 20);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(49, 13);
            this.label81.TabIndex = 1;
            this.label81.Text = "Prenume";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(30, 20);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(35, 13);
            this.label80.TabIndex = 0;
            this.label80.Text = "Nume";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.label79);
            this.groupBox13.Controls.Add(this.textBox18);
            this.groupBox13.Controls.Add(this.textBox17);
            this.groupBox13.Controls.Add(this.textBox16);
            this.groupBox13.Controls.Add(this.textBox15);
            this.groupBox13.Controls.Add(this.label78);
            this.groupBox13.Controls.Add(this.label77);
            this.groupBox13.Controls.Add(this.label76);
            this.groupBox13.Controls.Add(this.label75);
            this.groupBox13.Location = new System.Drawing.Point(534, 70);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(642, 239);
            this.groupBox13.TabIndex = 5;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Vehiculele Clientului";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(18, 42);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(616, 13);
            this.label79.TabIndex = 8;
            this.label79.Text = resources.GetString("label79.Text");
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(362, 58);
            this.textBox18.Multiline = true;
            this.textBox18.Name = "textBox18";
            this.textBox18.ReadOnly = true;
            this.textBox18.Size = new System.Drawing.Size(272, 172);
            this.textBox18.TabIndex = 7;
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(220, 58);
            this.textBox17.Multiline = true;
            this.textBox17.Name = "textBox17";
            this.textBox17.ReadOnly = true;
            this.textBox17.Size = new System.Drawing.Size(122, 172);
            this.textBox17.TabIndex = 6;
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(123, 58);
            this.textBox16.Multiline = true;
            this.textBox16.Name = "textBox16";
            this.textBox16.ReadOnly = true;
            this.textBox16.Size = new System.Drawing.Size(82, 172);
            this.textBox16.TabIndex = 5;
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(19, 58);
            this.textBox15.Multiline = true;
            this.textBox15.Name = "textBox15";
            this.textBox15.ReadOnly = true;
            this.textBox15.Size = new System.Drawing.Size(82, 172);
            this.textBox15.TabIndex = 4;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(457, 29);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(44, 13);
            this.label78.TabIndex = 3;
            this.label78.Text = "NrPolita";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(243, 29);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(57, 13);
            this.label77.TabIndex = 2;
            this.label77.Text = "SerieSasiu";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(133, 29);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(58, 13);
            this.label76.TabIndex = 1;
            this.label76.Text = "ModelAuto";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(30, 29);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(37, 13);
            this.label75.TabIndex = 0;
            this.label75.Text = "Marca";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.textBox14);
            this.groupBox12.Controls.Add(this.textBox13);
            this.groupBox12.Controls.Add(this.textBox12);
            this.groupBox12.Controls.Add(this.textBox11);
            this.groupBox12.Controls.Add(this.textBox10);
            this.groupBox12.Controls.Add(this.textBox9);
            this.groupBox12.Controls.Add(this.textBox8);
            this.groupBox12.Controls.Add(this.textBox7);
            this.groupBox12.Controls.Add(this.textBox6);
            this.groupBox12.Controls.Add(this.label74);
            this.groupBox12.Controls.Add(this.label73);
            this.groupBox12.Controls.Add(this.label72);
            this.groupBox12.Controls.Add(this.label71);
            this.groupBox12.Controls.Add(this.label70);
            this.groupBox12.Controls.Add(this.label69);
            this.groupBox12.Controls.Add(this.label68);
            this.groupBox12.Controls.Add(this.label67);
            this.groupBox12.Controls.Add(this.label66);
            this.groupBox12.Location = new System.Drawing.Point(20, 341);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(396, 183);
            this.groupBox12.TabIndex = 4;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Adresa Client";
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(280, 107);
            this.textBox14.Name = "textBox14";
            this.textBox14.ReadOnly = true;
            this.textBox14.Size = new System.Drawing.Size(100, 20);
            this.textBox14.TabIndex = 17;
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(280, 78);
            this.textBox13.Name = "textBox13";
            this.textBox13.ReadOnly = true;
            this.textBox13.Size = new System.Drawing.Size(100, 20);
            this.textBox13.TabIndex = 16;
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(280, 47);
            this.textBox12.Name = "textBox12";
            this.textBox12.ReadOnly = true;
            this.textBox12.Size = new System.Drawing.Size(100, 20);
            this.textBox12.TabIndex = 15;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(280, 17);
            this.textBox11.Name = "textBox11";
            this.textBox11.ReadOnly = true;
            this.textBox11.Size = new System.Drawing.Size(100, 20);
            this.textBox11.TabIndex = 14;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(75, 139);
            this.textBox10.Name = "textBox10";
            this.textBox10.ReadOnly = true;
            this.textBox10.Size = new System.Drawing.Size(100, 20);
            this.textBox10.TabIndex = 13;
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(75, 107);
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.Size = new System.Drawing.Size(100, 20);
            this.textBox9.TabIndex = 12;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(75, 78);
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(100, 20);
            this.textBox8.TabIndex = 11;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(75, 47);
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(100, 20);
            this.textBox7.TabIndex = 10;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(75, 17);
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(100, 20);
            this.textBox6.TabIndex = 9;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(205, 110);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(61, 13);
            this.label74.TabIndex = 8;
            this.label74.Text = "Apartament";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(205, 81);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(25, 13);
            this.label73.TabIndex = 7;
            this.label73.Text = "Etaj";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(205, 50);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(35, 13);
            this.label72.TabIndex = 6;
            this.label72.Text = "Scara";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(205, 20);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(28, 13);
            this.label71.TabIndex = 5;
            this.label71.Text = "Bloc";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(10, 142);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(38, 13);
            this.label70.TabIndex = 4;
            this.label70.Text = "Numar";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(10, 110);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(38, 13);
            this.label69.TabIndex = 3;
            this.label69.Text = "Strada";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(10, 81);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(53, 13);
            this.label68.TabIndex = 2;
            this.label68.Text = "Localitate";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(10, 50);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(33, 13);
            this.label67.TabIndex = 1;
            this.label67.Text = "Judet";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(7, 20);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(29, 13);
            this.label66.TabIndex = 0;
            this.label66.Text = "Tara";
            // 
            // Afiseaza
            // 
            this.Afiseaza.Location = new System.Drawing.Point(249, 11);
            this.Afiseaza.Name = "Afiseaza";
            this.Afiseaza.Size = new System.Drawing.Size(75, 23);
            this.Afiseaza.TabIndex = 3;
            this.Afiseaza.Text = "Afiseaza";
            this.Afiseaza.UseVisualStyleBackColor = true;
            this.Afiseaza.Click += new System.EventHandler(this.Afiseaza_Click);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label65);
            this.groupBox11.Controls.Add(this.label64);
            this.groupBox11.Controls.Add(this.label63);
            this.groupBox11.Controls.Add(this.label62);
            this.groupBox11.Controls.Add(this.label61);
            this.groupBox11.Controls.Add(this.textBox5);
            this.groupBox11.Controls.Add(this.textBox4);
            this.groupBox11.Controls.Add(this.textBox3);
            this.groupBox11.Controls.Add(this.textBox2);
            this.groupBox11.Controls.Add(this.textBox1);
            this.groupBox11.Location = new System.Drawing.Point(20, 70);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(380, 251);
            this.groupBox11.TabIndex = 2;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Detalii Client";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(26, 213);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(60, 13);
            this.label65.TabIndex = 9;
            this.label65.Text = "Nr. Telefon";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(26, 174);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(32, 13);
            this.label64.TabIndex = 8;
            this.label64.Text = "Email";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(26, 129);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(77, 13);
            this.label63.TabIndex = 7;
            this.label63.Text = "An Obt. Permis";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(26, 83);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(49, 13);
            this.label62.TabIndex = 6;
            this.label62.Text = "Prenume";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(26, 38);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(35, 13);
            this.label61.TabIndex = 5;
            this.label61.Text = "Nume";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(130, 210);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(199, 20);
            this.textBox5.TabIndex = 4;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(130, 171);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(199, 20);
            this.textBox4.TabIndex = 3;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(130, 126);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(199, 20);
            this.textBox3.TabIndex = 2;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(130, 80);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(199, 20);
            this.textBox2.TabIndex = 1;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(130, 35);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(199, 20);
            this.textBox1.TabIndex = 0;
            // 
            // cnp_detalii
            // 
            this.cnp_detalii.Location = new System.Drawing.Point(79, 13);
            this.cnp_detalii.Name = "cnp_detalii";
            this.cnp_detalii.Size = new System.Drawing.Size(158, 20);
            this.cnp_detalii.TabIndex = 1;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(17, 16);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(29, 13);
            this.label60.TabIndex = 0;
            this.label60.Text = "CNP";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.button4);
            this.tabPage5.Controls.Add(this.button3);
            this.tabPage5.Controls.Add(this.groupBox18);
            this.tabPage5.Controls.Add(this.groupBox17);
            this.tabPage5.Controls.Add(this.groupBox16);
            this.tabPage5.Controls.Add(this.groupBox15);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(1269, 539);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Date generale 1";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(1184, 405);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 37);
            this.button4.TabIndex = 5;
            this.button4.Text = "Sterge Polite Expirate";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(1184, 352);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 37);
            this.button3.TabIndex = 4;
            this.button3.Text = "Afiseaza polite";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.textBox36);
            this.groupBox18.Controls.Add(this.textBox35);
            this.groupBox18.Controls.Add(this.textBox34);
            this.groupBox18.Controls.Add(this.textBox33);
            this.groupBox18.Controls.Add(this.label99);
            this.groupBox18.Controls.Add(this.label100);
            this.groupBox18.Controls.Add(this.label101);
            this.groupBox18.Controls.Add(this.label102);
            this.groupBox18.Controls.Add(this.label103);
            this.groupBox18.Location = new System.Drawing.Point(611, 305);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(567, 231);
            this.groupBox18.TabIndex = 3;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Polite Care Nu AuExpirat";
            // 
            // textBox36
            // 
            this.textBox36.Location = new System.Drawing.Point(464, 48);
            this.textBox36.Multiline = true;
            this.textBox36.Name = "textBox36";
            this.textBox36.ReadOnly = true;
            this.textBox36.Size = new System.Drawing.Size(100, 177);
            this.textBox36.TabIndex = 13;
            // 
            // textBox35
            // 
            this.textBox35.Location = new System.Drawing.Point(313, 48);
            this.textBox35.Multiline = true;
            this.textBox35.Name = "textBox35";
            this.textBox35.ReadOnly = true;
            this.textBox35.Size = new System.Drawing.Size(145, 177);
            this.textBox35.TabIndex = 12;
            // 
            // textBox34
            // 
            this.textBox34.Location = new System.Drawing.Point(239, 48);
            this.textBox34.Multiline = true;
            this.textBox34.Name = "textBox34";
            this.textBox34.ReadOnly = true;
            this.textBox34.Size = new System.Drawing.Size(68, 177);
            this.textBox34.TabIndex = 11;
            // 
            // textBox33
            // 
            this.textBox33.Location = new System.Drawing.Point(7, 49);
            this.textBox33.Multiline = true;
            this.textBox33.Name = "textBox33";
            this.textBox33.ReadOnly = true;
            this.textBox33.Size = new System.Drawing.Size(226, 176);
            this.textBox33.TabIndex = 10;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(0, 32);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(568, 13);
            this.label99.TabIndex = 9;
            this.label99.Text = "---------------------------------------------------------------------------------" +
    "--------------------------------------------------------------------------------" +
    "--------------------------";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(487, 19);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(29, 13);
            this.label100.TabIndex = 8;
            this.label100.Text = "CNP";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(359, 19);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(35, 13);
            this.label101.TabIndex = 7;
            this.label101.Text = "Nume";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(236, 19);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(71, 13);
            this.label102.TabIndex = 6;
            this.label102.Text = "Data Expirate";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(74, 19);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(44, 13);
            this.label103.TabIndex = 5;
            this.label103.Text = "NrPolita";
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.textBox32);
            this.groupBox17.Controls.Add(this.textBox31);
            this.groupBox17.Controls.Add(this.textBox30);
            this.groupBox17.Controls.Add(this.textBox29);
            this.groupBox17.Controls.Add(this.label98);
            this.groupBox17.Controls.Add(this.label97);
            this.groupBox17.Controls.Add(this.label96);
            this.groupBox17.Controls.Add(this.label95);
            this.groupBox17.Controls.Add(this.label94);
            this.groupBox17.Location = new System.Drawing.Point(21, 305);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(584, 231);
            this.groupBox17.TabIndex = 2;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Polite Expirate";
            // 
            // textBox32
            // 
            this.textBox32.Location = new System.Drawing.Point(468, 48);
            this.textBox32.Multiline = true;
            this.textBox32.Name = "textBox32";
            this.textBox32.ReadOnly = true;
            this.textBox32.Size = new System.Drawing.Size(110, 177);
            this.textBox32.TabIndex = 8;
            // 
            // textBox31
            // 
            this.textBox31.Location = new System.Drawing.Point(311, 48);
            this.textBox31.Multiline = true;
            this.textBox31.Name = "textBox31";
            this.textBox31.ReadOnly = true;
            this.textBox31.Size = new System.Drawing.Size(151, 177);
            this.textBox31.TabIndex = 7;
            // 
            // textBox30
            // 
            this.textBox30.Location = new System.Drawing.Point(240, 48);
            this.textBox30.Multiline = true;
            this.textBox30.Name = "textBox30";
            this.textBox30.ReadOnly = true;
            this.textBox30.Size = new System.Drawing.Size(65, 177);
            this.textBox30.TabIndex = 6;
            // 
            // textBox29
            // 
            this.textBox29.Location = new System.Drawing.Point(15, 49);
            this.textBox29.Multiline = true;
            this.textBox29.Name = "textBox29";
            this.textBox29.ReadOnly = true;
            this.textBox29.Size = new System.Drawing.Size(219, 176);
            this.textBox29.TabIndex = 5;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(12, 32);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(568, 13);
            this.label98.TabIndex = 4;
            this.label98.Text = "---------------------------------------------------------------------------------" +
    "--------------------------------------------------------------------------------" +
    "--------------------------";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(499, 19);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(29, 13);
            this.label97.TabIndex = 3;
            this.label97.Text = "CNP";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(364, 16);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(35, 13);
            this.label96.TabIndex = 2;
            this.label96.Text = "Nume";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(234, 19);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(71, 13);
            this.label95.TabIndex = 1;
            this.label95.Text = "Data Expirate";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(86, 19);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(44, 13);
            this.label94.TabIndex = 0;
            this.label94.Text = "NrPolita";
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.label93);
            this.groupBox16.Controls.Add(this.button2);
            this.groupBox16.Controls.Add(this.CNP_benef_Afis);
            this.groupBox16.Controls.Add(this.textBox28);
            this.groupBox16.Controls.Add(this.textBox27);
            this.groupBox16.Controls.Add(this.textBox26);
            this.groupBox16.Controls.Add(this.label92);
            this.groupBox16.Controls.Add(this.label91);
            this.groupBox16.Controls.Add(this.label90);
            this.groupBox16.Controls.Add(this.label89);
            this.groupBox16.Location = new System.Drawing.Point(636, 14);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(596, 270);
            this.groupBox16.TabIndex = 1;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Benefactor";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(281, 246);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(79, 13);
            this.label93.TabIndex = 9;
            this.label93.Text = "CNP Beneficiar";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(512, 240);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "Afiseaza";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // CNP_benef_Afis
            // 
            this.CNP_benef_Afis.Location = new System.Drawing.Point(378, 243);
            this.CNP_benef_Afis.Name = "CNP_benef_Afis";
            this.CNP_benef_Afis.Size = new System.Drawing.Size(114, 20);
            this.CNP_benef_Afis.TabIndex = 7;
            // 
            // textBox28
            // 
            this.textBox28.Location = new System.Drawing.Point(337, 42);
            this.textBox28.Multiline = true;
            this.textBox28.Name = "textBox28";
            this.textBox28.ReadOnly = true;
            this.textBox28.Size = new System.Drawing.Size(250, 185);
            this.textBox28.TabIndex = 6;
            // 
            // textBox27
            // 
            this.textBox27.Location = new System.Drawing.Point(172, 41);
            this.textBox27.Multiline = true;
            this.textBox27.Name = "textBox27";
            this.textBox27.ReadOnly = true;
            this.textBox27.Size = new System.Drawing.Size(123, 186);
            this.textBox27.TabIndex = 5;
            // 
            // textBox26
            // 
            this.textBox26.Location = new System.Drawing.Point(10, 42);
            this.textBox26.Multiline = true;
            this.textBox26.Name = "textBox26";
            this.textBox26.ReadOnly = true;
            this.textBox26.Size = new System.Drawing.Size(126, 185);
            this.textBox26.TabIndex = 4;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(7, 25);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(580, 13);
            this.label92.TabIndex = 3;
            this.label92.Text = "---------------------------------------------------------------------------------" +
    "--------------------------------------------------------------------------------" +
    "------------------------------";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(445, 13);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(47, 13);
            this.label91.TabIndex = 2;
            this.label91.Text = "Nr.Polita";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(210, 12);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(49, 13);
            this.label90.TabIndex = 1;
            this.label90.Text = "Prenume";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(49, 12);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(35, 13);
            this.label89.TabIndex = 0;
            this.label89.Text = "Nume";
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.button1);
            this.groupBox15.Controls.Add(this.label88);
            this.groupBox15.Controls.Add(this.label87);
            this.groupBox15.Controls.Add(this.label86);
            this.groupBox15.Controls.Add(this.label85);
            this.groupBox15.Controls.Add(this.textBox25);
            this.groupBox15.Controls.Add(this.textBox24);
            this.groupBox15.Controls.Add(this.textBox23);
            this.groupBox15.Location = new System.Drawing.Point(36, 14);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(563, 276);
            this.groupBox15.TabIndex = 0;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Clienti Carora li s-a folosit polita";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(482, 247);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Afiseaza";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(33, 31);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(514, 13);
            this.label88.TabIndex = 6;
            this.label88.Text = "---------------------------------------------------------------------------------" +
    "--------------------------------------------------------------------------------" +
    "--------";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(435, 16);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(29, 13);
            this.label87.TabIndex = 5;
            this.label87.Text = "CNP";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(260, 15);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(49, 13);
            this.label86.TabIndex = 4;
            this.label86.Text = "Prenume";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(80, 16);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(35, 13);
            this.label85.TabIndex = 3;
            this.label85.Text = "Nume";
            // 
            // textBox25
            // 
            this.textBox25.Location = new System.Drawing.Point(408, 47);
            this.textBox25.Multiline = true;
            this.textBox25.Name = "textBox25";
            this.textBox25.ReadOnly = true;
            this.textBox25.Size = new System.Drawing.Size(119, 186);
            this.textBox25.TabIndex = 2;
            // 
            // textBox24
            // 
            this.textBox24.Location = new System.Drawing.Point(225, 47);
            this.textBox24.Multiline = true;
            this.textBox24.Name = "textBox24";
            this.textBox24.ReadOnly = true;
            this.textBox24.Size = new System.Drawing.Size(135, 186);
            this.textBox24.TabIndex = 1;
            // 
            // textBox23
            // 
            this.textBox23.Location = new System.Drawing.Point(46, 47);
            this.textBox23.Multiline = true;
            this.textBox23.Name = "textBox23";
            this.textBox23.ReadOnly = true;
            this.textBox23.Size = new System.Drawing.Size(132, 186);
            this.textBox23.TabIndex = 0;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.button8);
            this.tabPage6.Controls.Add(this.groupBox19);
            this.tabPage6.Controls.Add(this.textBox39);
            this.tabPage6.Controls.Add(this.label107);
            this.tabPage6.Controls.Add(this.label106);
            this.tabPage6.Controls.Add(this.button7);
            this.tabPage6.Controls.Add(this.vScrollBar1);
            this.tabPage6.Controls.Add(this.textBox38);
            this.tabPage6.Controls.Add(this.button6);
            this.tabPage6.Controls.Add(this.label105);
            this.tabPage6.Controls.Add(this.textBox37);
            this.tabPage6.Controls.Add(this.label104);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(1269, 539);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Delete&Update";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(772, 67);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 11;
            this.button8.Text = "Actualizeaza";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.textBox40);
            this.groupBox19.Controls.Add(this.textBox41);
            this.groupBox19.Controls.Add(this.textBox42);
            this.groupBox19.Controls.Add(this.textBox43);
            this.groupBox19.Controls.Add(this.textBox44);
            this.groupBox19.Controls.Add(this.textBox45);
            this.groupBox19.Controls.Add(this.textBox46);
            this.groupBox19.Controls.Add(this.textBox47);
            this.groupBox19.Controls.Add(this.textBox48);
            this.groupBox19.Controls.Add(this.label108);
            this.groupBox19.Controls.Add(this.label109);
            this.groupBox19.Controls.Add(this.label110);
            this.groupBox19.Controls.Add(this.label111);
            this.groupBox19.Controls.Add(this.label112);
            this.groupBox19.Controls.Add(this.label113);
            this.groupBox19.Controls.Add(this.label114);
            this.groupBox19.Controls.Add(this.label115);
            this.groupBox19.Controls.Add(this.label116);
            this.groupBox19.Location = new System.Drawing.Point(492, 124);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(396, 269);
            this.groupBox19.TabIndex = 10;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Adresa Client";
            // 
            // textBox40
            // 
            this.textBox40.Location = new System.Drawing.Point(280, 158);
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new System.Drawing.Size(100, 20);
            this.textBox40.TabIndex = 17;
            // 
            // textBox41
            // 
            this.textBox41.Location = new System.Drawing.Point(280, 112);
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(100, 20);
            this.textBox41.TabIndex = 16;
            // 
            // textBox42
            // 
            this.textBox42.Location = new System.Drawing.Point(280, 64);
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(100, 20);
            this.textBox42.TabIndex = 15;
            // 
            // textBox43
            // 
            this.textBox43.Location = new System.Drawing.Point(280, 17);
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(100, 20);
            this.textBox43.TabIndex = 14;
            // 
            // textBox44
            // 
            this.textBox44.Location = new System.Drawing.Point(75, 213);
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new System.Drawing.Size(100, 20);
            this.textBox44.TabIndex = 13;
            // 
            // textBox45
            // 
            this.textBox45.Location = new System.Drawing.Point(75, 158);
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new System.Drawing.Size(100, 20);
            this.textBox45.TabIndex = 12;
            // 
            // textBox46
            // 
            this.textBox46.Location = new System.Drawing.Point(75, 112);
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new System.Drawing.Size(100, 20);
            this.textBox46.TabIndex = 11;
            // 
            // textBox47
            // 
            this.textBox47.Location = new System.Drawing.Point(75, 64);
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new System.Drawing.Size(100, 20);
            this.textBox47.TabIndex = 10;
            // 
            // textBox48
            // 
            this.textBox48.Location = new System.Drawing.Point(75, 17);
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new System.Drawing.Size(100, 20);
            this.textBox48.TabIndex = 9;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(205, 161);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(61, 13);
            this.label108.TabIndex = 8;
            this.label108.Text = "Apartament";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(205, 115);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(25, 13);
            this.label109.TabIndex = 7;
            this.label109.Text = "Etaj";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(205, 67);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(35, 13);
            this.label110.TabIndex = 6;
            this.label110.Text = "Scara";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(205, 20);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(28, 13);
            this.label111.TabIndex = 5;
            this.label111.Text = "Bloc";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(7, 216);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(38, 13);
            this.label112.TabIndex = 4;
            this.label112.Text = "Numar";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(7, 161);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(38, 13);
            this.label113.TabIndex = 3;
            this.label113.Text = "Strada";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(7, 115);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(53, 13);
            this.label114.TabIndex = 2;
            this.label114.Text = "Localitate";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(7, 67);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(33, 13);
            this.label115.TabIndex = 1;
            this.label115.Text = "Judet";
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(7, 20);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(29, 13);
            this.label116.TabIndex = 0;
            this.label116.Text = "Tara";
            // 
            // textBox39
            // 
            this.textBox39.Location = new System.Drawing.Point(553, 70);
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(130, 20);
            this.textBox39.TabIndex = 9;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(489, 73);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(58, 13);
            this.label107.TabIndex = 8;
            this.label107.Text = "CNP Client";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(486, 27);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(284, 26);
            this.label106.TabIndex = 7;
            this.label106.Text = "Schimbare adresa pentru un client existent in cazul in care \r\nacesta in schimba d" +
    "omiciliul";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(301, 64);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(112, 23);
            this.button7.TabIndex = 6;
            this.button7.Text = "Emina Acoperiri";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // vScrollBar1
            // 
            this.vScrollBar1.Location = new System.Drawing.Point(394, 93);
            this.vScrollBar1.Name = "vScrollBar1";
            this.vScrollBar1.Size = new System.Drawing.Size(19, 426);
            this.vScrollBar1.TabIndex = 5;
            // 
            // textBox38
            // 
            this.textBox38.Location = new System.Drawing.Point(26, 93);
            this.textBox38.Multiline = true;
            this.textBox38.Name = "textBox38";
            this.textBox38.ReadOnly = true;
            this.textBox38.Size = new System.Drawing.Size(387, 426);
            this.textBox38.TabIndex = 4;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(301, 27);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(112, 23);
            this.button6.TabIndex = 3;
            this.button6.Text = "Afiseaza";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(49, 12);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(194, 26);
            this.label105.TabIndex = 2;
            this.label105.Text = "Afisare acoperiri efectuate pe o anumita\r\n polita, care apoi se pot sterge";
            // 
            // textBox37
            // 
            this.textBox37.Location = new System.Drawing.Point(76, 52);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(219, 20);
            this.textBox37.TabIndex = 1;
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(23, 55);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(47, 13);
            this.label104.TabIndex = 0;
            this.label104.Text = "Nr.Polita";
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(10, 94);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(55, 13);
            this.label117.TabIndex = 6;
            this.label117.Text = "Executant";
            // 
            // textBox49
            // 
            this.textBox49.Location = new System.Drawing.Point(100, 91);
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new System.Drawing.Size(200, 20);
            this.textBox49.TabIndex = 7;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.textBox50);
            this.tabPage7.Controls.Add(this.button12);
            this.tabPage7.Controls.Add(this.label121);
            this.tabPage7.Controls.Add(this.button11);
            this.tabPage7.Controls.Add(this.label120);
            this.tabPage7.Controls.Add(this.button10);
            this.tabPage7.Controls.Add(this.label119);
            this.tabPage7.Controls.Add(this.button9);
            this.tabPage7.Controls.Add(this.label118);
            this.tabPage7.Controls.Add(this.dataGridView1);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(1269, 539);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Date generale 2";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(549, 21);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(695, 497);
            this.dataGridView1.TabIndex = 0;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(38, 47);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(279, 13);
            this.label118.TabIndex = 1;
            this.label118.Text = "Persoanele care si-au reparat masinile de mai mult de 2-ori";
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(417, 47);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 2;
            this.button9.Text = "Afiseaza";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(38, 123);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(226, 26);
            this.label119.TabIndex = 3;
            this.label119.Text = "Polite care nu au fost folosite pentru a putea fi \r\nacoperite\r\n";
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(417, 118);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 23);
            this.button10.TabIndex = 4;
            this.button10.Text = "Afiseaza";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(38, 193);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(230, 13);
            this.label120.TabIndex = 5;
            this.label120.Text = "Politele care au fost folosite de mai mult de 2-ori";
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(417, 188);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 23);
            this.button11.TabIndex = 6;
            this.button11.Text = "Afiseaza";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(41, 265);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(217, 26);
            this.label121.TabIndex = 7;
            this.label121.Text = "Afisare polita/polite vehicul, plus proprietarul \r\nin functie de serie sasiu";
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(417, 259);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 23);
            this.button12.TabIndex = 8;
            this.button12.Text = "Afiseaza";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // textBox50
            // 
            this.textBox50.Location = new System.Drawing.Point(264, 262);
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new System.Drawing.Size(146, 20);
            this.textBox50.TabIndex = 9;
            this.textBox50.TextChanged += new System.EventHandler(this.textBox50_TextChanged);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(353, 436);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(75, 23);
            this.button13.TabIndex = 8;
            this.button13.Text = "Strge lista polite";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1301, 584);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Gestiunea politelor RCA la un broker de asigurari";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox tara;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nrTelefon;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox email;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox cnp;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox prenume;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox nume;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox apartament;
        private System.Windows.Forms.TextBox etaj;
        private System.Windows.Forms.TextBox scara;
        private System.Windows.Forms.TextBox bloc;
        private System.Windows.Forms.TextBox numar;
        private System.Windows.Forms.TextBox strada;
        private System.Windows.Forms.TextBox localitate;
        private System.Windows.Forms.TextBox judet;
        private System.Windows.Forms.Button Adauga;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox serieCIV;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox numarLocuri;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox combustibil;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox putere;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox capacitateCilindrica;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox masaMaxima;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox serieSasiu;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox numarInmatriculare;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox model;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox marca;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox valoareTotala;
        private System.Windows.Forms.DateTimePicker dataCreare;
        private System.Windows.Forms.DateTimePicker dataExpirare;
        private System.Windows.Forms.DateTimePicker dataEfectiva;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.TextBox cnp_client_existent;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox anulFabricatiei;
        private System.Windows.Forms.TextBox an_obt_permis;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label nrVehicule;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Button adaugaVehicul;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox politaDeExtins;
        private System.Windows.Forms.TextBox cnpClientAfisPolite;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button afiseazaPoliteClient;
        private System.Windows.Forms.TextBox valoareAfisat;
        private System.Windows.Forms.TextBox dataExpirareAfisat;
        private System.Windows.Forms.TextBox dataCreareEfectiva;
        private System.Windows.Forms.TextBox dataCreareAfisat;
        private System.Windows.Forms.TextBox NrPolitaAfisat;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox valAdaug;
        private System.Windows.Forms.Button PrelungirePerioada;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.DateTimePicker perioadaNouaExt;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button AdaugaPolitaAcoperire;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox nrPolitaAcoperire;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.TextBox nrTelefonBeneficiar;
        private System.Windows.Forms.TextBox EmailBeneficiar;
        private System.Windows.Forms.TextBox cnpBeneficiar;
        private System.Windows.Forms.TextBox prenumeBeneficiar;
        private System.Windows.Forms.TextBox numeBeneficiar;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.DateTimePicker dataAcoperire;
        private System.Windows.Forms.TextBox denumireAcoperire;
        private System.Windows.Forms.TextBox descriereAcoperire;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Button utilizeazaPolita;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox cnp_detalii;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Button Afiseaza;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox CNP_benef_Afis;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.VScrollBar vScrollBar1;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.TextBox textBox49;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox textBox50;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Button button13;
    }
}

