﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectBD.Service
{
    public class ExistentUserException: Exception
    {
        public ExistentUserException()
        {
        }
        public ExistentUserException(string message)
            : base(message)
        {
        }

        public ExistentUserException(string message, Exception innerException)
            : base(message, innerException)
        {

        }
    }
}
