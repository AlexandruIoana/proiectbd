﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectBD.Service
{
    public class NonExistentPolitaException : Exception
    {
        public NonExistentPolitaException()
        {
        }
        public NonExistentPolitaException(string message)
            : base(message)
        {
        }

        public NonExistentPolitaException(string message, Exception innerException)
            : base(message, innerException)
        {

        }
    }
}
