﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectBD.Service
{
    public class ExpiredPolitaException : Exception
    {
        public ExpiredPolitaException()
        {
        }

        public ExpiredPolitaException(string message)
            : base(message)
        {
        }

        public ExpiredPolitaException(string message, Exception innerException)
            : base(message, innerException)
        {

        }
    }
}
