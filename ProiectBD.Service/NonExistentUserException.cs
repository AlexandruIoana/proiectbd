﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectBD.Service
{
    public class NonExistentUserException : Exception
    {
        public NonExistentUserException()
        {
        }
        public NonExistentUserException(string message)
            : base(message)
        {
        }

        public NonExistentUserException(string message, Exception innerException)
            : base(message, innerException)
        {

        }
    }
}
