﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ProiectBD.Classes;
using System.Configuration;
using System.Data.SqlClient;

namespace ProiectBD.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class Rca_Service : iRca_Service
    {
        private string connectionString;
        private SqlConnection con;
        private SqlDataReader dr;
        private SqlCommand cmd;

        private Random rnd;
    
        public Rca_Service()
        {
            connectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
     
            con = new SqlConnection(connectionString);
            rnd = new Random();
        }

        public int GeyIdByCNP(string cnp)
        {
            int id = 0;
            string aux = null;

            try
            {
                con.Open();
                string commnad = string.Format("SELECT Id FROM[BazaDeDateProiectBD].[dbo].[Clienti_PersoanaFizica] where CNP = '{0}'", cnp);
                cmd = new SqlCommand(commnad, con);
                dr = cmd.ExecuteReader();

                if (dr.HasRows == true)
                {
                    while (dr.Read())
                    {
                        aux = dr[0].ToString();
                    }
                    id = Int32.Parse(aux);
                }
                return id;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }
        public int GetIdMasinaBySerieSasiu(string serieSasiu)
        {
            int id = 0;
            string aux = null;

            try
            {
                con.Open();
                string commnad = string.Format("SELECT Id FROM[BazaDeDateProiectBD].[dbo].[Vehicule] where SerieSasiu = '{0}'", serieSasiu);
                cmd = new SqlCommand(commnad, con);
                dr = cmd.ExecuteReader();

                if (dr.HasRows == true)
                {
                    while (dr.Read())
                    {
                        aux = dr[0].ToString();
                    }
                    id = Int32.Parse(aux);
                }
                return id;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }
        public void executeQuery(string command)
        {
            try
            {
                con.Open();
                
                cmd = new SqlCommand(command, con);
                dr = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }
        public void AchizitiePolitaClientExistent(string cnp, List<Vehicul> vehicule, Polita polita)
        {
            int id,id_masina,idPolita;
            string command;
            
            id = GeyIdByCNP(cnp);
            if(id == 0)
            {
                throw new NonExistentUserException("Acest Client nu exista in baza noastra de date.");
           }
           else
           {
                command =string.Format("insert into [BazaDeDateProiectBD].[dbo].[Polite] (NumarPolita, DataEfectiva, DataExpirare, ValoareTotala, DataCreare, ClientId)  values('{0}','{1}','{2}',{3},'{4}',{5})", polita.NumarPolita,polita.DataEfectiva,polita.DataExpirare,polita.ValoareTotala,polita.DataCreare,id);
                executeQuery(command);

                idPolita = GetIdPolitaByNrPolita(polita.NumarPolita);
                foreach( var car in vehicule)
                {
                    id_masina = GetIdMasinaBySerieSasiu(car.SerieSasiu);
                    if (id_masina == 0)
                    {
                        command = string.Format("INSERT INTO [BazaDeDateProiectBD].[dbo].[Vehicule] (Marca ,ModelAuto ,NumarDeInmatriculare ,SerieSasiu ,AnulProductiei ,MasaMaxima, CapacitateCilindrica ,Putere ,TipDeCombustibil , NumarLocuri , SerieCIV , ProprietarId ) values('{0}', '{1}', '{2}', '{3}', {4}, {5}, {6}, {7}, '{8}', {9}, '{10}' ,{11})", car.Marca, car.ModelAuto ,car.NumarDeInmatriculare,car.SerieSasiu,car.AnulProductiei,car.MasaMaxima,car.CapacitateCilindrica,  car.Putere,car.TipDeCombustibil,car.NumarDeLocuri,car.SerieCIV,id);
                        executeQuery(command);
                    }
                    
                    id_masina = GetIdMasinaBySerieSasiu(car.SerieSasiu);
                    command = string.Format("INSERT INTO [BazaDeDateProiectBD].[dbo].[Polite_Vehicule]([PolitaId], [VehiculId]) values({0},{1})", idPolita , id_masina);
                    executeQuery(command);
                }
            }
        }

        public void AchizitiePolitaClientNou(Client client, List<Vehicul> vehicule, Polita polita)
        {
            string command;
            int id;
            id = GeyIdByCNP(client.CNP);
            if (id == 0)
            {
               
                command = string.Format("insert into [BazaDeDateProiectBD].[dbo].[Clienti_PersoanaFizica] ( Nume, Prenume, CNP, AnulObtineriiPermisului, Email, NumarTelefon) values('{0}', '{1}', '{2}', {3}, '{4}', '{5}')", client.Nume, client.Prenume, client.CNP, client.AnulObtineriiPermisului, client.Email,client.NumarTelefon);
                executeQuery(command);

                id = GeyIdByCNP(client.CNP);
                command = string.Format("insert into [BazaDeDateProiectBD].[dbo].[Clienti_Adrese] (ClientId,Tara,Judet,Localitate,Strada,Numar,Bloc,Scara,Etaj,Apartament) values( {0}, '{1}' , '{2}' , '{3}' , '{4}' , '{5}' , '{6}' , '{7}' , '{8}' , '{9}' )",  id, client.Adresa.Tara, client.Adresa.Judet, client.Adresa.Localitate, client.Adresa.Strada,                                                                                      (client.Adresa.Numar == null)? DBNull.Value.ToString() : client.Adresa.Numar.ToString() ,                                                          (client.Adresa.Bloc == null)? DBNull.Value.ToString() : client.Adresa.Bloc.ToString(),                                                        (client.Adresa.Scara == null)? DBNull.Value.ToString() : client.Adresa.Scara.ToString() ,                                                         (client.Adresa.Etaj == null) ? DBNull.Value.ToString() : client.Adresa.Etaj.ToString(),                                               (client.Adresa.Apartament==null)? DBNull.Value.ToString() : client.Adresa.Apartament.ToString());
                executeQuery(command);
                AchizitiePolitaClientExistent(client.CNP, vehicule, polita);
            }
            else
            {
                AchizitiePolitaClientExistent(client.CNP, vehicule, polita);
            }
        }

        public List<PoliteAfisClient> GetPoliteByCnp(string cnp)
        {
            List<PoliteAfisClient> lista = new List<PoliteAfisClient>();
            PoliteAfisClient item = new PoliteAfisClient();

            string commnad = string.Format("SELECT NumarPolita,DataEfectiva,DataExpirare,ValoareTotala,DataCreare FROM [BazaDeDateProiectBD].[dbo].[Polite] WHERE ClientId = (SELECT Id FROM [BazaDeDateProiectBD].[dbo].[Clienti_PersoanaFizica] WHERE CNP='{0}')",cnp);
            try
            {
                con.Open();
                cmd = new SqlCommand(commnad, con);
                dr = cmd.ExecuteReader();

                if (dr.HasRows == true)
                {
                    while (dr.Read())
                    {
                        item = new PoliteAfisClient();
                        item.NrPolita = dr[0].ToString();
                        item.DataEfectiva = dr[1].ToString().Substring(0 , dr[1].ToString().IndexOf(" "));
                        item.DataExpirare = dr[2].ToString().Substring(0 , dr[2].ToString().IndexOf(" "));
                        item.Valoare = dr[3].ToString();
                        item.DataCreare = dr[4].ToString().Substring(0 , dr[4].ToString().IndexOf(" "));
                        lista.Add(item);
                    }
                    return lista;
                }
                else
                {
                    throw new NonExistentUserException("User-ul nu exista in baza de date");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private int GetIdPolitaByNrPolita(string nrPolita)
        {
            int id = 0;
            string aux = null;

            try
            {
                con.Open();
                string commnad = string.Format("SELECT Id FROM [BazaDeDateProiectBD].[dbo].[Polite] where NumarPolita = '{0}'", nrPolita);
                cmd = new SqlCommand(commnad, con);
                dr = cmd.ExecuteReader();

                if (dr.HasRows == true)
                {
                    while (dr.Read())
                    {
                        aux = dr[0].ToString();
                    }
                    id = Int32.Parse(aux);
                }
                return id;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }
        public void ExtindeDataExpirarePolita(string numarrPolita, string nouaData, double addValoare)
        {
            string command;
            int id = GetIdPolitaByNrPolita(numarrPolita);

            if(id==0)
            {
                throw new NonExistentPolitaException("Polita nu exista in baza noastra de date");
            }
            else
            {
                command = string.Format("UPDATE [BazaDeDateProiectBD].[dbo].[Polite] SET DataExpirare = '{0}' , ValoareTotala = ValoareTotala + {1}  WHERE NumarPolita = '{2}'", nouaData, addValoare, numarrPolita);
                executeQuery(command);
            }
        }

        public void UtilizeazaPolita(List<string> lista, Acoperire acoperire, Beneficiar beneficiar)
        {
            int idPolita;
            int id=0;
            int idAcoperire;
            string command;

            id = GetIdBeneficiarByCNP(beneficiar.CNP);
            if (id == 0)
            {
                command = string.Format("INSERT INTO [BazaDeDateProiectBD].[dbo].[Beneficiar] ([Nume] , [Prenume] , [CNP] , [Email] , [NumarTelefon]) VALUES ('{0}' , '{1}' , '{2}' , '{3}' ,'{4}')", beneficiar.Nume, beneficiar.Prenume, beneficiar.CNP, beneficiar.Email, beneficiar.NumarTelefon);
                executeQuery(command);
            }

            if(id==0) id = GetIdBeneficiarByCNP(beneficiar.CNP);

            command = String.Format("INSERT INTO [BazaDeDateProiectBD].[dbo].[Acoperire] ( NumeAcoperire , Data , Descriere , BeneficiarId,Executant) VALUES ( '{0}' ,'{1}' ,'{2}', {3},'{4}')", acoperire.Denumire, acoperire.dataAcoperire, acoperire.Descriere, id,acoperire.Executant);
            executeQuery(command);

            idAcoperire = GetIdAcoperire(id, acoperire.Denumire, acoperire.dataAcoperire);

            foreach (var nrPolita in lista)
            {
                idPolita = GetIdPolitaByNrPolita(nrPolita);
                if(idPolita == 0)
                {
                    throw new NonExistentPolitaException("Polita nu exista");
                }
                
                if(IsExpiredPolita(nrPolita) == true)
                {
                    if (lista.Count == 1)
                         throw new ExpiredPolitaException("Polita este expirata!!!");
                }
                else
                {
                        command = string.Format("INSERT INTO [BazaDeDateProiectBD].[dbo].[Polite_Acoperire] ( [PolitaId] ,[AcoperireId]) VALUES ({0} ,{1})",idPolita,idAcoperire);
                        executeQuery(command);
                 }
                
            }
        }

        private int GetIdAcoperire(int idBeneficiar, string denumire, string dataAcoperire)
        {
            int id = 0;
            string aux=null;
            string command = String.Format("SELECT  [Id] FROM[BazaDeDateProiectBD].[dbo].[Acoperire] WHERE ( BeneficiarId={0} AND NumeAcoperire='{1}' AND Data LIKE '{2}')",idBeneficiar,denumire,dataAcoperire);

            try
            {
                con.Open();
                cmd = new SqlCommand(command, con);
                dr = cmd.ExecuteReader();

                if (dr.HasRows == true)
                {
                    while (dr.Read())
                    {
                        aux = dr[0].ToString();
                    }
                    id = Int32.Parse(aux);
                }
                return id;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private int GetIdBeneficiarByCNP(string cnp)
        {
            int id = 0;
            string aux = null;

            try
            {
                con.Open();
                string commnad = string.Format("SELECT Id FROM [BazaDeDateProiectBD].[dbo].[Beneficiar] where CNP = '{0}'", cnp);
                cmd = new SqlCommand(commnad, con);
                dr = cmd.ExecuteReader();

                if (dr.HasRows == true)
                {
                    while (dr.Read())
                    {
                        aux = dr[0].ToString();
                    }
                    id = Int32.Parse(aux);
                }
                return id;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private bool IsExpiredPolita(string nrPolita)
        {
            string command = string.Format("SELECT DataExpirare FROM [BazaDeDateProiectBD].[dbo].[Polite] WHERE NumarPolita='{0}'",nrPolita);
            string aux;
            DateTime date = DateTime.Now; 
            try
            {
                con.Open();
                cmd = new SqlCommand(command, con);
                dr = cmd.ExecuteReader();

                if (dr.HasRows == true)
                {
                    while (dr.Read())
                    {
                        aux = dr[0].ToString();
                        date = Convert.ToDateTime(aux);

                    }

                    if (date < DateTime.Now)
                        return true;
                    else
                        return false;
                }
                else
                    throw new NonExistentPolitaException("Polita nu exista in baza de date");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        public void GetDataClient(string cnp, ref Client client, ref Adresa adresa)
        {
            client = new Client();
            adresa = new Adresa();
            VehiculPolita v = new VehiculPolita();
            Beneficiar beneficiar = new Beneficiar();

            string command;
            int idClient = 0;

            ////////////Client/////////////////
            command = string.Format("SELECT Id , Nume , Prenume  , AnulObtineriiPermisului, Email , NumarTelefon FROM [BazaDeDateProiectBD].[dbo].[Clienti_PersoanaFizica] WHERE CNP='{0}'", cnp);
            try
            {
                con.Open();
                cmd = new SqlCommand(command, con);
                dr = cmd.ExecuteReader();
                if (dr.HasRows == true)
                {
                    while (dr.Read())
                    {
                        idClient = Convert.ToInt32(dr[0].ToString());
                        client.Nume = dr[1].ToString();
                        client.Prenume = dr[2].ToString();
                        client.AnulObtineriiPermisului = Convert.ToInt32(dr[3].ToString());
                        client.Email = dr[4].ToString();
                        client.NumarTelefon = dr[5].ToString();
                    }
                }
                else
                {
                    throw new NonExistentUserException("Userul nu exista");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
            //////////////////Adresa////////////////
            command = string.Format("SELECT Tara , Judet , Localitate , Strada , Numar , Bloc, Scara , Etaj , Apartament FROM[BazaDeDateProiectBD].[dbo].[Clienti_Adrese] WHERE ClientId=(SELECT Id FROM [BazaDeDateProiectBD].[dbo].[Clienti_PersoanaFizica] WHERE CNP='{0}')", cnp);
            try
            {
                con.Open();
                cmd = new SqlCommand(command, con);
                dr = cmd.ExecuteReader();
                if (dr.HasRows == true)
                {
                    while (dr.Read())
                    {
                        adresa.Tara = dr[0].ToString();
                        adresa.Judet = dr[1].ToString();
                        adresa.Localitate = dr[2].ToString();
                        adresa.Strada = dr[3].ToString();
                        adresa.Numar = Convert.ToInt32(dr[4].ToString());
                        adresa.Bloc = Convert.ToInt32(dr[5].ToString());
                        adresa.Scara = Convert.ToChar(dr[6].ToString());
                        adresa.Etaj = Convert.ToInt32(dr[7].ToString());
                        adresa.Apartament = Convert.ToInt32(dr[8].ToString());
                    }
                }
                else
                {
                    throw new NonExistentUserException("Userul nu exista");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }

        }

        public void EliminatePoliteExpirate()
        {
            string command = "DELETE FROM [BazaDeDateProiectBD].[dbo].[Polite] WHERE DataExpirare < CONVERT(date, getdate())";
            executeQuery(command);
        }

        /*////////////////////////////
         ////////////////////////////
         /////////////////////////////*/
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
    }
}
