﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using ProiectBD.Service;

namespace ProiectBD.Queries
{
    class Program
    {
        static void GetClientByCnp()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["ProiectBD"].ConnectionString;
            SqlConnection con;
            SqlDataReader dr;
            SqlCommand cmd;
            con = new SqlConnection(connectionString);


            int id = 0;
            string aux = null;
            string cnp = "1950921384967";
            try
            {
                con.Open();
                string commnad = string.Format("SELECT Id FROM[BazaDeDateProiectBD].[dbo].[Clienti_PersoanaFizica] where CNP = {0}", cnp);
                cmd = new SqlCommand(commnad, con);
                dr = cmd.ExecuteReader();

                if (dr.HasRows == true)
                {
                    while (dr.Read())
                    {
                        aux = dr[0].ToString();
                    }
                    id = Int32.Parse(aux);
                }
                else
                {
                    Console.WriteLine("User does not exist\n");
                }

                Console.WriteLine(id);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        static void metoda()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["ProiectBD"].ConnectionString;
            SqlConnection con;
            SqlDataReader dr;
            SqlCommand cmd;
            con = new SqlConnection(connectionString);


            int id = 0;
            string aux = null;
            string serieSasiu = "AAAAAAAAAAAAAAAA";
            try
            {
                con.Open();
                string commnad = string.Format("SELECT Id FROM[BazaDeDateProiectBD].[dbo].[Vehicule] where SerieSasiu = '{0}'", serieSasiu);
                cmd = new SqlCommand(commnad, con);
                dr = cmd.ExecuteReader();

                if (dr.HasRows == true)
                {
                    while (dr.Read())
                    {
                        aux = dr[0].ToString();
                    }
                    id = Int32.Parse(aux);
                    Console.WriteLine(id);
                }
                else
                    Console.WriteLine(id);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }
        static void Main(string[] args)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["ProiectBD"].ConnectionString;
            SqlConnection con;
            SqlDataReader dr;
            SqlCommand cmd;
            con = new SqlConnection(connectionString);
            string nrPolita = "bfd800ca-4e4d-4703-8f56-959962c67f2c";

            string command = string.Format("SELECT DataExpirare FROM[BazaDeDateProiectBD].[dbo].[Polite] WHERE NumarPolita='{0}'", nrPolita);
            string aux;
            DateTime date=DateTime.Now; ;
            try
            {
                con.Open();
                cmd = new SqlCommand(command, con);
                dr = cmd.ExecuteReader();

                if (dr.HasRows == true)
                {
                    while(dr.Read())
                    {
                        aux = dr[0].ToString();
                        date = Convert.ToDateTime(aux);

                    }
                    if (date < DateTime.Now)
                        Console.WriteLine("Polita e expirata");
                    else
                        Console.WriteLine("Polita nu e expirata");
                }
                else
                    throw new NonExistentPolitaException("Polita nu exista in baza de date");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        
    }
}
